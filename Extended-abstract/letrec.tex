\input{\jobname.cfg}
\documentclass[sigplan,9pt,screen,review,natbib,nonacm]{acmart}
\settopmatter{printfolios=true,printccs=false,printacmref=false}
\setcopyright{none}
\usepackage[utf8]{inputenc}

\usepackage{reversion}
\usepackage{mylistings}
\usepackage{mycomments}
\usepackage{mymath}
\usepackage{mybiblio}
\usepackage{myhyperref}
\usepackage{notations}
\usepackage{myrefs}

\Comments{}{\UNXXX}

% "long footnotes" are hidden when \Long is \False, but the counter is
% still incremented to preserve footnote numbers between the two
% versions
\newcommand{\longfootnote}[1]{\Long{\footnote{#1}}{\stepcounter{footnote}}}

\newcommand{\Title}{A right-to-left type system for value recursion}

\title{\Title}

\author{Alban Reynaud}
\affiliation{\institution{ENS Lyon, France}}
\author{Gabriel Scherer}
\affiliation{\institution{INRIA, France}}
\author{Jeremy Yallop}
\affiliation{\institution{University of Cambridge, UK}}

\begin{document}
\maketitle

%% \Draft{\hfill\textbf{Draft, \today}}{}

\begin{abstract}
In call-by-value languages, some mutually-recursive value definitions
can be safely evaluated to build recursive functions or cyclic data
structures, but some definitions (\lstinline`let rec x = x + 1`) contain
vicious circles and their evaluation fails at runtime. We propose
a new static analysis to check the absence of such runtime failures.

We present a set of declarative inference rules, prove its soundness
with respect to the reference source-level semantics of
\citet*{dynamic-semantics-2008}, and show that it can be
(right-to-left) directed into an algorithmic check in a surprisingly
simple way.

Our implementation of this new check replaced the existing check used
by the OCaml programming language, a fragile syntactic/grammatical
criterion which let several subtle bugs slip through as the language
kept evolving. We document some issues that arise when advanced
features of a real-world functional language (exceptions in
first-class modules, GADTs, etc.) interact with safety checking for
recursive definitions.

\end{abstract}

\section{Introduction}

In OCaml recursive functions are defined using the \lstinline|let rec|
operator, as in the following definition of \textit{factorial}:
%
\begin{lstlisting}
let rec fac x = if x = 0 then 1 else x * (fac (x - 1))
\end{lstlisting}

Beside functions, |let rec| can define recursive values, such as an
infinite list |ones| where every element is |1|:
\begin{lstlisting}
let rec ones = 1 :: ones
\end{lstlisting}
Note that this ``infinite'' list is actually cyclic, and consists of a
single cons-cell referencing itself.

However, not all recursive definitions can be computed. 
The following definition is justly rejected by the compiler:
\begin{lstlisting}
let rec x = 1 + x
\end{lstlisting}
Here \lstinline|x| is used in its own definition. Computing
\lstinline|1 + x| requires \lstinline|x| to have a known
value: this definition contains a vicious circle, and any
evaluation strategy would fail.

Functional languages deal with recursive values in various
ways. Standard ML simply rejects all recursive definitions except
function values. At the other extreme, Haskell accepts all well-typed
recursive definitions, including those that lead to infinite
computation. In OCaml, safe cyclic-value definitions are accepted, and
they are occasionally useful.

For example, consider an interpreter for a programming language with
datatypes for ASTs and for values:
\begin{lstlisting}
type ast = Fun of var * expr | $\ldots$
type value = Closure of env * var * expr | $\ldots$
\end{lstlisting}
%
The |eval| function builds values from environments and |ast|s
%
\begin{lstlisting}
let rec eval env = function
  | $\ldots$
  | Fun (x, t) -> Closure(env, x, t)
\end{lstlisting}
%
Now consider adding an |ast| constructor
%
\lstinline|FunRec of var * var * expr|
%
for recursive functions: \lstinline|FunRec ("f", "x", t)| represents
the recursive function $\letrecin {\app f x = t} f$. Our OCaml
interpreter can use value recursion to build a closure for these
recursive functions, without changing the definition of
\lstinline|Closure|: the recursive closure simply adds
itself to the closure environment (|(var * value) list|).
\begin{lstlisting}
let rec eval env = function
  | $\ldots$
  | Fun (x, t) -> Closure(env, x, t)
  | FunRec (f, x, t) ->
    let rec cl = Closure((f,cl)::env, x, t) in cl
\end{lstlisting}

\paragraph{Our new check and its implementation}
Until recently, the static check used by OCaml to reject vicious
definitions relied on a syntactic analysis, performed on an untyped
intermediate language . While we believe that the 
check as originally defined was correct, it proved fragile and hard to extend to the
interaction of new language features with recursive definitions. Over
the years, bugs were found where the check was unduly lenient.  In
conjunction with OCaml's efficient recursive definition compilation
scheme~\citep{hirschowitz-compilation-2009}, this leniency led to
segmentation faults.

Seeking to address these problems, we designed and implemented a new check
for recursive definition safety based on a novel static analysis,
formulated as a simple type system (which we have proved sound with
respect to an existing operational
semantics~\cite{dynamic-semantics-2008}), and implemented as part of
OCaml's type-checking phase.
%
Our check was merged into the OCaml distribution in
August 2018.  

Moving the check from the middle end to the type checker restores the
desirable property that \textit{compilation of well-typed programs
does not go wrong}.  This property is convenient for tools that reuse
OCaml's type-checker without performing compilation, such as
MetaOCaml~\cite{DBLP:conf/flops/Kiselyov14} (which type-checks quoted
code) and Merlin~\cite{DBLP:journals/pacmpl/BourRS18} (which
type-checks code during editing).  Furthermore, some aspects of the
check have delicate interactions with types, and so cannot be
performed on an untyped IR~(\S\ref{section:gadts}).


\paragraph{Our analysis}

We looked at reusing existing inference systems, but they do not
appear to suit our analysis: they have a finer-grained handling of
functions and functors than we need, but coarser-grained handling of
cyclic data, and most do not propose effective inference algorithms.
In return for a coarser analysis, our system is noticeably
simpler; furthermore, it scales cleanly to the full OCaml language.

A key aspect of our approach is the idea of right-to-left (type to
environment) algorithmic interpretation, which reduces complexity
compared to a presentation designed for a left-to-right reading. It is
novel in this space and could inspire other inference rules designers.

This article gives an overview of our approach.
%
A more detailed presentation of the system is given in the
accompanying full paper~\cite{fullpaper}.

\section{Static and dynamic semantics}

\begin{figure*}[t!]
\abovedisplayskip 0em
\belowdisplayskip 0em
\begin{minipage}{0.4\textwidth}
\begin{mathpar}
  \begin{array}{l@{~}r@{~}l}
    \set{Terms} \ni t, u & \bnfeq & x, y, z \\
    & \bnfor & \letrecin b u \\
    & \bnfor & \lam x t %  \\
    % &
      \bnfor % &
               \app t u \\
    & \bnfor & \constr K {\fam i {t_i}} % \\
    % &
      \bnfor % &
               \match t h \\ \\
  \end{array}

  \begin{array}{l@{~}r@{~}l}
    \set{Bindings} \ni b & \bnfeq & {\fam i {x_i = t_i}} \\
    % \\
    \set{Handlers} \ni h & \bnfeq & {\fam i {\clause {p_i} {t_i}}} \\
    % \\
    \set{Patterns} \ni p, q & \bnfeq & \constr K {\fam i {x_i}} \\
  \end{array}
%%   \begin{array}{l@{~}r@{~}l}
%%     \set{Terms} \ni t, u \; \bnfeq \; x, y, z 
%%     \; \bnfor \; \letrecin b u 
%%     \; \bnfor \; \lam x t %  \\
%%     % \;
%%       \bnfor % \;
%%                \app t u 
%%     \; \bnfor \; \constr K {\fam i {t_i}} % \\
%%     % \;
%%       \bnfor % \;
%%                \match t h
%%   \end{array}
%% \vspace{-1.5ex}

%%   \begin{array}{c@{~}c@{~}c}
%%   \begin{array}{l@{~}r@{~}l}
%%     \set{Bindings} \ni b & \bnfeq & {\fam i {x_i = t_i}} \\
%%   \end{array} &
%%   \begin{array}{l@{~}r@{~}l}
%%     \set{Handlers} \ni h & \bnfeq & {\fam i {\clause {p_i} {t_i}}} \\
%%   \end{array} &
%%   \begin{array}{l@{~}r@{~}l}
%%     \set{Patterns} \ni p, q & \bnfeq & \constr K {\fam i {x_i}} \\
%%   \end{array}
%%   \end{array}
\end{mathpar}
\caption{\label{fig:syntax}{Core language syntax}}
\end{minipage}%
\begin{minipage}{0.6\textwidth}
\begin{mathpar}
\begin{array}{l} \text{Modes:} \quad
\Ignore \prec \Delay \prec \Guard \prec \Return \prec \Dereference
\end{array}

\begin{array}{l} \text{Mode composition:} \\
\begin{array}{lccccccc}
\mcomp m {m'} & \Ignore & \Delay & \Guard       & \Return      & \Dereference & m \\
\hline
\Ignore      & \Ignore & \Ignore & \Ignore      & \Ignore      & \Ignore      \\
\Delay       & \Ignore & \Delay  & \Delay       & \Delay       & \Dereference \\
\Guard       & \Ignore & \Delay  & \Guard       & \Guard       & \Dereference \\
\Return      & \Ignore & \Delay  & \Guard       & \Return      & \Dereference \\
\Dereference & \Ignore & \Delay  & \Dereference & \Dereference & \Dereference \\
m' &
\end{array}
\end{array}
\end{mathpar}

\caption{\label{fig:modes}{Access modes and operations}}
\end{minipage}


\begin{mathpar}
  %% subsumption
  \infer
  {\der \Gamma t m\\
   m \succ m'}
  {\der \Gamma t {m'}}

  %% lambda
  \infer
  {\der {\Gamma, \of x {m_x}} t {\mcomp m \Delay}}
  {\der \Gamma {\lam x t} m}

  %% application
  \infer
  {\der {\Gamma_t} t {\mcomp m \Dereference} \\
   \der {\Gamma_u} u {\mcomp m \Dereference}}
  {\der {\envsum {\Gamma_t} {\Gamma_u}} {\app t u} m}

  %% let-rec
  \parbox{0.5\textwidth}{
  \infer
  {\fam {i \in I}
    {\der
      {\Gamma_i, \fam {j \in I} {\of {x_j} {m_{i,j}}}}
      {t_i}
      \Return}
   \\
   \fam {i,j} {m_{i,j} \preceq \Guard}
   \\\\
   \fam i {\Gamma'_i =
     \envsum {\Gamma_i} {\envbigsum {\fam j {\mcomp {m_{i,j}} {\Gamma'_j}}}}}
  }
  {\derbinding
    {\fam {i \in I} {\of {x_i} {\Gamma'_i}}}
    {\fam {i \in I} {x_i = t_i}}}
  }\parbox{0.05\textwidth}{~}\parbox{0.45\textwidth}{
  \infer
  {\derbinding {\fam i {\of {x_i} {\Gamma_i}}} b \\
   \fam i {m'_i} \defeq \fam i {\max(m_i, \Guard)} \\
   \der {\Gamma_u, \fam i {\of {x_i} {m_i}}} u m}
  {\der
   {\envsum
     {\envbigsum {\fam i {\mcomp {m'_i} {\Gamma_i}}}}
     {\Gamma_u}}
   {\letrecin b u}
   m}
  }
\end{mathpar}
\caption{\label{fig:rules}Mode inference rules (abridged)}
\end{figure*}

\paragraph{Syntax}
Figure~\ref{fig:syntax} introduces a minimal subset of ML with
the interesting ingredients of OCaml's recursive value definitions:
a multi-ary \code{let rec} binding $\letrecin {\fam i {x_i = t_i}} u$,
functions ($\lambda$-abstractions) $\lam x t$ 
and applications $t u$,
datatype constructors $\constr K (t_1, t_2, \dots)$ and shallow pattern-matching
  $\match t {\fam i {\clause {\constr {K_i} {\fam j {x_{i,j}}}} {u_i}}}$.

Other ML constructs (non-recursive |let|, tuples, conditionals, etc.)
can be desugared into this core.
In fact, the full
inference rules for OCaml (and our check) exactly correspond to the
rules (and check) derived from this desugaring.
%% \begin{itemize}
%% \item $n$-ary tuples are a special case of constructors:\\
%%   $(t_1, t_2, \dots, t_n)$ desugars into
%%   $\constr {\Tuple_n} {\fam {i \in [1;n]} {t_i}}$.
%% \item Non-recursive \emph{let} bindings are recursive bindings with
%%   access mode $\Ignore$:\\ $\letin x t u$ desugars into
%%   $\letrecin {x = t} u$.
%% \item Conditionals are a special case of pattern-matching:\\
%%   $\ifte t {u_1} {u_2}$ desugars into
%%   $\match t {(\clause {\kwd{True}} {u_1} \mid \clause {\kwd{False}} {u_2})}$.
%%  % \item Sequencing: $\seq t u$ desugars into
%% %   $\match t (\clause \wild u)$.  \Xgabriel{Due to the discarding
%% %     issue, $t$ is $\Return$ in this desugaring, not $\Guard$.}
%% \end{itemize}

Since ML's types are largely orthogonal to our analysis, we present
the check using an untyped fragment.  (In the full OCaml language,
there are some interactions with types --- in particular, with GADTs
--- see \S\ref{section:gadts}.)
%
Although we ignore types, we do assume that terms are well-scoped --- n.b.~in~$\letrecin {\fam i {x_i = v_i}} u$, the $\fam i {x_i}$ are in
scope of $u$ but also of all the $v_i$.

\paragraph{Access modes}
\label{subsec:intro-modes}

For each recursive binding $x = e$, our analysis assigns
an \textit{access mode} $m$ representing the way that $x$ is 
accessed during evaluation of $e$.

Figure~\ref{fig:modes} defines the modes, their order structure,
and the mode composition operations.
%
The modes are as follows:
\begin{description}
\item[$\Ignore$]: an expression is entirely unused during
  the evaluation of the program. This is the mode of a variable
  in an expression in which it does not occur.
\item[$\Delay$]: a context can be evaluated (to Weak Normal Form) without evaluating its argument. $\lam x \hole$ is
  a delay context.
\item [$\Guard$]: the context returns the value as a member
  of a data structure (e.g.~a variant or
  record). $\constr K {(\hole)}$ is a guard context. The value can
  safely be defined mutually-recursively with its context, as in
  $\letrec {x = {\constr K {(x)}}}$\footnote{
  $\Guard$ is also used for terms whose result is discarded by the context.
   For example, $\hole$ is in a
  $\Guard$ context in $\letin x \hole u$, if $x$ is not used in
  $u$. Such terms cannot create self-loops, so we consider them guarded.}.
\item [$\Return$]: the context returns its value without
  further inspection. This value cannot be defined
  mutually-recursively with its context, to avoid self-loops: in
  $\letrec {x = x}$ and $\letrec {x = \letin y x y}$, the last
occurrence of $x$  is in $\Return$ context.
\item [$\Dereference$]: the context inspects and
  uses the value in arbitrary ways. Such a value must be fully defined
  at the point of usage; it cannot be defined mutually-recursively
  with its context. $\match \hole h$ is a $\Dereference$ context.
\end{description}

The ordering $m \prec m'$ places less demanding, more permissive modes
that do not involve dereferencing variables, below more demanding,
less permissive modes.

Each mode is closely associated with particular expression contexts.
%
For example, $\app t \hole$ is a $\Dereference$ context, since 
$t$ may access its argument in arbitrary ways, while $\lam x \hole$ is a
$\Delay$ context.

Mode composition corresponds to context composition:
if an expression context $\plug E \hole$ uses its hole at mode $m$,
and a second context $\plug {E'} \hole$ uses its hole at
mode $m'$, then the composed context $\plug E {\plug {E'} \hole}$
uses its hole at mode $\mcomp m {m'}$.
%
Like context composition, mode composition is associative, but not
commutative: $\mcomp \Dereference \Delay$ is
$\Dereference$, but $\mcomp \Delay \Dereference$ is $\Delay$.

%% Taking into account associativity, identity, idempotence and annihilation leaves 6 combinations
%% \begin{itemize}
%% \item \verb!Guard[Delay] = Delay! / \verb!Delay[Guard] = Delay!
%% \item \verb!Guard[Deref] = Deref! / \verb!Deref[Guard] = Deref!
%% \item \verb!Delay[Deref] = Delay! / \verb!Deref[Delay] = Deref!
%% \end{itemize}

Continuing the example above, the context
$\app t {(\lam x \hole)}$,
formed by composing $\app t \hole$ and $\lam x \hole$,
is a $\Dereference$ context: the intuition is that the function $t$ may
pass an argument to its input and then access the result in arbitrary
ways.
%
In contrast, the context $\lam x {(\app t \hole)}$,
formed by composing $\lam x \hole$ and $\app t \hole$,
is a $\Delay$ context: the contents of the hole will
not be touched before the abstraction is applied.

Finally, $\Ignore$ is the absorbing element of mode composition
($\mcomp m \Ignore = \Ignore = \mcomp \Ignore m$), $\Return$ is
an identity ($\mcomp \Return m = m = \mcomp m \Return$),
and composition is idempotent ($\mcomp m m = m$).

\paragraph{A right-to-left inference system}
\label{subsec:intro-right-to-left}
\label{subsec:intro-rules}

Figure~\ref{fig:rules} gives a representative sample of the inference rules
for a judgment of form $\der \Gamma t m$ for
term $t$, access mode $m$ and environment $\Gamma$ that maps term
variables to access modes. Modes classify terms and variables, playing
the role of types in usual type systems. The example judgment $
\der {x : \Dereference, y : \Delay} {(x+1, \lazy{y})} \Guard
$
can be read either
\begin{description}
\item[left-to-right:] If $x$ can safely be used in
  $\Dereference$ mode, and $y$ in $\Delay$ mode,
  then $(x+1, \lazy{y})$ can safely be used at $\Guard$.
\item[right-to-left:] If a context accesses the term
  $(x+1, \lazy{y})$ under mode $\Guard$, then $x$ is accessed at
  $\Dereference$, and $y$ at $\Delay$.
\end{description}
%
\hspace{\parindent}
This judgment uses access modes to classify both variables and the
constraints imposed on a term by its surrounding context. If 
$C[\hole]$ uses its hole $\hole$ at the mode $m$, then any
derivation for $C[t] : \Return$ will contain a sub-derivation of the
form $t : m$.

Mode composition features in each term rule: if we try to
prove $C[t] : m'$, then the sub-derivation will check
$t : \mcomp {m'} m$, where $\mcomp {m'} m$ is the composition of the
access-mode $m$ under a surrounding access mode $m'$, and $\Return$ is
neutral for composition.

Our judgment $\der \Gamma t m$ can be directed into an algorithm
following our right-to-left interpretation. Given a term $t$ and an
mode $m$ as inputs, our algorithm computes the least demanding environment
$\Gamma$ such that $\der \Gamma t m$ holds.

For example, the rule for abstraction in Figure~\ref{fig:rules} has
the following right-to-left reading: to compute the constraints
$\Gamma$ on $\lam x t$ in a context of mode $m$, it suffices to check
the body $t$ under the weaker mode $\mcomp m \Delay$, and remove the
variable $x$ from the collected constraints -- its mode does not
matter. If $t$ is a variable $y$ and $m$ is $\Return$, the resulting
environment is $\of y \Delay$.

Given a family of mutually-recursive definitions
$\letrecfam {i \in I} {x_i} {t_i}$, we run our algorithm on each $t_i$
at the mode $\Return$, and obtain a family of environments
$\fam{i \in I}{\Gamma_i}$ such that all the judgments
$\fam{i \in I}{\der {\Gamma_i} {t_i} \Return}$ hold. The definitions
are rejected if any $\Gamma_i$ contains one of the
mutually-defined $x_j$ under the mode $\Dereference$ or
$\Return$ rather than $\Guard$ or $\Delay$.

%% We remind the reader that the algorithmic interpretation of the rules
%% is right-to-left: the term judgment $\der \Gamma t m$ involves
%% constructing the environment $\Gamma$ based on known $t$ and $m$.

%% \fbox{Binding judgment $\derbinding {\fam i {\of {x_i} {\Gamma_i}}} b$}

\subparagraph*{Subsumption}

We have a subtyping/subsumption rule; for example, if we want to check
$t$ under the mode $\Guard$, it is always sound 
to attempt
to check it under the stronger mode $\Dereference$.
%
More generally, $m \succ m'$ means that $m$ is \textit{more demanding}
than $m'$, which means (in the usual subtyping sense) that it
classifies \emph{fewer} terms; a proof of
$\der \Gamma t m$ suffices to conclude $\der \Gamma t m'$. 
%
Our algorithmic check does not use this rule; it is here for
completeness: it allows to derive some judgments that are intuitively
correct, but are less precise than those inferred by our algorithms.

\subparagraph*{Abstraction and application}

The rule for abstraction is discussed above.
%
The application rule checks both function and argument in
a $\Dereference$ context, and merges the two resulting environments,
taking the most demanding mode on each side; a variable $y$
is dereferenced by $\app t u$ if it is dereferenced by either $t$ or
$u$.
%
The constructor rule (elided; it may be found in the full paper) is
similar, but constructor parameters appear in $\Guard$ context, rather
than $\Dereference$.

\subparagraph*{Recursive definitions} The rule for mutually-recursive
definitions $\letrecin b u$ is split into two parts with disjoint
responsibilities. First, the binding judgment
$\derbinding {\fam i {\of {x_i} {\Gamma_i}}} b$ computes, for each
definition $x_i = e_i$ in a recursive binding $b$, the usage
$\Gamma_i$ of the ambient context before the binding -- we
detail its definition below.

Second, the $\letrecin b u$ rule of the term judgment takes these
$\Gamma_i$ and uses them under a composition
$\mcomp {m'_i} {\Gamma_i}$, to account for the actual access mode of
the variables. (Here $\mcomp m \Gamma$ denotes the pointwise lifting of
composition for each mode in $\Gamma$.) The access mode $m'_i$ is
a combination of the access mode in the body $u$ and $\Guard$, used to
indicate that our eager language will compute the values now,
even if they are not used in $u$, or only under a delay.

\subparagraph*{Binding judgment and mutual recursion}
The \emph{binding judgment}
$\derbinding {\fam {i \in I} {\of {x_i} {\Gamma_i}}} b$ is independent
of the ambient context and access mode; it checks recursive bindings
in isolation in the $\Return$ mode, and relates each name $x_i$
introduced by the binding $b$ to an environment $\Gamma_i$ on the
ambient free variables.

In the first premise, for each binding $(x_i = t_i)$ in $b$, we check
the term $t_i$ in a context split in two parts, some usage context
$\Gamma_i$ on the ambient context around the recursive definition, and
a context $\fam {j \in I} {\of {x_j} {m_{i,j}}}$ for the
recursively-bound variables, where $m_{i,j}$ is the mode of use of
$x_j$ in the definition of $x_i$.

The second premise checks that the modes $m_{i,j}$ are
$\preceq \Guard$, to ensure that these mutually-recursive definitions
are valid.

The third premise makes mutual-recursion safe by turning the
$\Gamma_i$ into bigger contexts $\Gamma'_i$ taking transitive mutual
dependencies into account: if a definition $x_i = e_i$ uses
the mutually-defined variable $x_j$ under the mode $m_{i,j}$, then we
ask that the final environment $\Gamma'_i$ for $e_i$ contains what you
need to use $e_j$ under the mode $m_{i,j} $, that is
$\mcomp {m_{i,j}} {\Gamma'_j}$. This set of equations
corresponds to the fixed point of a monotone function, so
it has a unique least solution.

Note: because the $m_{i,j}$ must be below $\Guard$, we can show
that $\mcomp {m_{i,j}} {\Gamma_j} \preceq \Gamma_j$. In particular, if
we have a single recursive binding, we have
$\Gamma_i \succeq \mcomp {m_{i,i}} {\Gamma_i}$, so the third premise
is equivalent to just $\Gamma'_i \defeq \Gamma_i$: the $\Gamma'_i$ and
$\Gamma_i$ only differ for non-trivial mutual recursion.

The full paper develops meta-theoretic properties of
our inference rules, such as principality.

\paragraph{Right-to-left in general} Typing rules are a specialized
declarative language to describe and justify various computational
processes related to a type system (type checking, type inference,
elaboration, etc.). Our right-to-left reading is one possible way to
describe the static analysis we are capturing, which could also be
described in many other ways: as pseudocode, as a fixpoint of
equations, through a denotational semantics, etc. In general we
believe that right-to-left readings can give a nice, compact,
declarative presentation of certain demand analyses, in a language
that type designers are already familiar with.

\section{Meta-theory: soundness}
\label{section:meta-theory}

The full paper connects our inference rules to the
operational semantics of \citet*{dynamic-semantics-2008}, with a more
detailed consideration of what it means for a term to go wrong (which
turns out to be quite subtle).

We define a notion of \emph{forcing context} --- a context that really
accesses the value of its hole --- and a \emph{vicious} term as
one with a forcing context containing a variable whose definition has
not been evaluated.  Then we show soundness via the following theorems:

\begin{lemma}[Forcing-deref]
  \label{lem:forcing-dereference} If, for a forcing context ${\forcing
  E}$, $\der {\Gamma, \of x m} {\plug {\forcing E} x} \Return$ is
  derivable, then $m$ is $\Dereference$.
\end{lemma}

\begin{theorem}[Vicious]\label{lem:vicious}
  $\der \emptyset t \Return$ never holds for $t \in \set{Vicious}$.
\end{theorem}

\begin{theorem}[Subj.red.]\label{thm:subject-reduction}
  If $\der \Gamma t m$ and $\rew t {t'}$ then $\der \Gamma {t'} m$.
\end{theorem}

\begin{corollary}
  $\Return$-typed programs cannot go vicious.
\end{corollary}


\section{Extension to a full language: GADTs}
\label{section:gadts}

The combination of  efficient compilation, non-uniform
value representation, and features (GADTs, first-class modules) with
subtle interactions between types and values introduces several challenges
for checking recursive definitions in the full OCaml language.
%
We sketch how our system naturally extends to one such challenge.

At the point where the original syntactic check took place, on an
untyped IR quite late in the compiler pipeline, exhaustive
single-clause matches such as  \lstinline|match t with () -> $\ldots$|)
had been transformed into direct substitutions.
%
With this design,  programs of the following form are accepted:
\begin{lstlisting}
  type t = Foo
  let rec x = match x with Foo -> Foo
\end{lstlisting}

This appears innocuous, but it becomes unsound with the
addition of GADTs to the language~\cite{gadt-bug}:
\begin{lstlisting}
type (_, _) eq = Refl : ('a, 'a) eq
let all_eq (type a b) : (a, b) eq =
 let rec (p: (a,b) eq) = match p with Refl$\,$->$\,$Refl in$\,$p
\end{lstlisting}
%
For the GADT |eq|, matching against \lstinline{Refl} is not a no-op:
it brings a type equality into scope that increases the number of
types that can be assigned to the
program~\citep{DBLP:conf/aplas/GarrigueR13}.
%
It is therefore necessary to treat matches involving GADTs as
inspections to ensure that a value of the appropriate type is actually
available; without that change definitions such as
\lstinline{all_eq} violate type safety.

% \begin{verbatim}
% - GADT refl: there was a subtle bug in the original criterion due to
%   ignoring single-match cases: GADT constructors really need to be
%   dereferenced otherwise the type equality may be invalid
% \end{verbatim}

% \Xgabriel{(Is this bug only about the static/dynamic size
%   classification?)}

% \Xjeremy{I think this is refering to PR 7215, which isn't about sizes.  See below.}

% \begin{verbatim}
% type (_,_) eq = Refl : ('a, 'a) eq
% let cast (type a) (type b) (Refl : (a, b) eq) (x : a) = (x : b)
% let is_int (type a) =
%   let rec (p : (int, a) eq) = match p with Refl -> Refl in
%   p
% let bang = print_string (cast (is_int : (int, string) eq) 42)
% \end{verbatim}

% The |is_int| function is compiled to the equivalent of
% |let rec p = () in p|, I think because the single-constructor
% match of |p| against |Refl| is optimized away in the
% untyped middle end.

% (One way to look at this is that |Refl| isn't really
% nullary, since it carries a hidden type equality argument, which is
% actually used on the rhs of the match, so |let rec| should
% treat the match as a proper dereference.)

% This is all rather like the fact that GADT matching needs to be strict
% (to avoid treating bottom as a valid witness) in lazy languages.


\section{Closing remarks}

We have presented a new static analysis for recursive value
declarations, designed to solve a fragility issue in the OCaml
language semantics and implementation.
%
It is less expressive than previous works that analyze function calls
in a fine-grained way; in return, it remains fairly simple, despite
its ability to scale to a fully-fledged programming language, and the
constraint of having a direct correspondence with a simple inference
algorithm.

We believe that this analysis may be of use for other
functional languages, both typed and untyped.
%
It seems likely that the techniques we have used will
apply to other systems --- type parameter variance, type constructor
roles, and so on.
%
Our hope in describing our system is that we will
eventually see a pattern emerge for designing
``things that look like type systems'' in this way.

For reasons of space we refer the reader to the full
paper~\cite{fullpaper} for a discussion of related work.



\bibliography{letrec}


\end{document}

