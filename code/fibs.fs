type ('k,'v) memofun = { f: 'k -> 'v;  mutable values: ('k * 'v) list  }
let remember t k =
    try snd (List.find (fst >> ((=) k)) t.values)
    with Not_found -> let v = t.f k in 
                      t.values <- (k, v) :: t.values;
                      v

let rec mfib = fun x -> if x <= 1 then x
                         else remember mfibs (x-1) + remember mfibs (x-2)
    and mfibs = { f = mfib; values = [] }
(* ok *)


let rec mfib' = let mfibs' = { f = mfib'; values = empty_table () } in
                fun x -> if x <= 1 then x
                         else remember mfibs' (x-1) + remember mfibs' (x-2)


let rec efibs = 0 :: 1 :: map2 (+) efibs (tail efibs)
(* rejected *)



let rec mfib = { f = mfib; values = [] }
    and mfibs = fun x -> if x <= 1 then x
                         else remember mfibs (x-1) + remember mfibs (x-2)
