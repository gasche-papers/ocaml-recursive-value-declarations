type 'a llist' = Nil | (::) of 'a * 'a llist
and 'a llist = 'a llist' Lazy.t

let rec map2 f x y =
  lazy (match x, y with
        | _, lazy Nil | lazy Nil, _ -> Nil
        | lazy ((x :: xs)), lazy ( (y :: ys)) -> ( (f x y :: map2 f xs ys)))
let rec nth n l =
  match n, l with 0, lazy (x :: _) -> x | n, lazy (_ :: xs) -> nth (pred n) xs
let tail x = lazy (let lazy ( (_::tl)) = x in Lazy.force tl);;
let rec fibs = lazy (1 :: lazy (1 :: map2 (+) fibs (tail fibs)))


let rec fibs = lazy (0 :: lazy (1 :: map2 (+) fibs (tail fibs)))

let rec fib = fun x -> if x <= 1 then x
                          else fib (x-1) + fib (x-2)


module StrictMemo =
struct
  type ('k,'v) memotab = { f: 'k -> 'v; values: ('k, 'v) Hashtbl.t }

  (* val remember : ('k, 'v) memotab -> 'k -> 'v *)
  let remember t k =
  try Hashtbl.find t.values k
  with Not_found -> let v = t.f k in Hashtbl.add t.values k v; v

  (* NB: this does not memoize the outermost call *)
  let rec fib = fun x -> if x <= 1 then x
                         else remember fibs (x-1) + remember fibs (x-2)
      and fibs = { f = fib; values = Hashtbl.create 10 }

  let rec fib2 = let fibs = { f = fib2; values = Hashtbl.create 10 } in
                 fun x -> if x <= 1 then x
                          else remember fibs (x-1) + remember fibs (x-2)

  let fibs3 = let rec fib = fun x -> if x <= 1 then x
                                     else remember fibs (x-1) + remember fibs (x-2)
              and fibs = { f = fib; values = Hashtbl.create 10 } in
              remember fibs
                  
end
