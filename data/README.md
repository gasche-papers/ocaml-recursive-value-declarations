A list of occurrences of non-standard recursion (i.e. of recursive
bindings that would be disallowed in Standard ML), extracted by
running opamcheck with an instrumented compiler.

What's here:
* `rec.log` logfile containing instances of non-standard recursion
* `stats.py` script that parses & analyses the logfile and displays statistics
* `letrec-instrument.patch`: patch against OCaml 4.10.0 that detects and logs
  instances of non-standard recursion
* `run`: instructions for building and running a Docker image that runs
  `opamcheck` with the patched compiler.


### Summary

We found 398 (textually-)distinct instances of non-standard recursion
in around 96 packages (253 packages if we include all the Oasis
`setup.ml` files).
