module Ocaml_ast = Migrate_parsetree.Ast_410

let parse_expression lexbuf =
  match Ocaml_common.Parse.expression lexbuf with
  | exception exn -> Error exn
  | ast ->
    let convert =
      let open Migrate_parsetree.Versions in
      (migrate ocaml_current ocaml_410).copy_expression in
    Ok (convert ast)

let rec easily_safe expr =
  let open Ocaml_ast.Parsetree in
  match expr.pexp_desc with
  | Pexp_ident _ -> true

  (* only literal constants, no complex constants *)
  | Pexp_constant _ -> true

  | Pexp_fun _ | Pexp_function _ -> true

  | Pexp_open (_, e)       (* let open Foo in ... *)
  | Pexp_newtype (_, e)    (* fun (type t) -> ... *)
  | Pexp_constraint (e, _) (* (... : t) *)
    -> easily_safe e

  | Pexp_let (_rec_flag, bindings, e) ->
    let simple vb = easily_safe vb.pvb_expr in
    List.for_all simple bindings && easily_safe e

  | _ -> false

let () =
  let lexbuf = Lexing.from_channel stdin in
  match parse_expression lexbuf with
  | Error exn ->
    Printf.eprintf "error: %S" (Printexc.to_string exn)
  | Ok expr ->
    print_endline
      (if easily_safe expr then "easy" else "hard")
