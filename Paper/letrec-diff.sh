set -x
git show before-revision-work:./letrec.tex > letrec-before-revisions.tex
echo '\nonstopmode' > letrec-diff.tex
latexdiff -t cfont --allow-spaces --math-markup=whole letrec-before-revisions.tex letrec.tex >> letrec-diff.tex
cp letrec.cfg letrec-diff.cfg
