# Revision plan: submission #349

## Format

We divide the document into two sections:
  1. Comments we commit to addressing.
  2. Comments we plan not to address

There is a third category, of comments from the reviewers that we
believe don't need a commitment.  We are carefully considering
comments in that category, and anticipate addressing the majority of
them in the revision.  However, our impression is that the reviewers
do not wish to insist on those points, and so we have not committed to
addressing them in any particular way.

Quotations are marked to indicate the source:
  * `[A]` from Review A
  * `[B]` from Review B
  * `[C]` from Review C
  * `[X]` from the comment accompanying the notification
  * `[R]` from the author response

## 1. Comments we commit to addressing

### 1.1 Title

[B]
> Title: Although accurate, the title does not distinguish the work
> from, say, Dreyer 2004 ("A Type System for Well-founded Recursion").

This point is well-taken.  We're considering the title carefully, and
are minded to follow recent suggestions changing "type system" to
"mode system" and adding "practical".

### 1.2 Backpatching

[A]
> * Lines 706ff, and 748.  You are very dismissive of these readers,
>   presenting Section 6 as a concession to the feeble-minded. But
>   there's a Very Good Reason that your readers want section 6: the
>   operational semantics of Fig 4 *is not what is executed*.
[...]
[B]
> *Operational Semantices*
> 
> My secondary reservation is about the choice of a local-store
> operational semantics; the paper gets bogged down by a defense of ---
> and extra technical development due to --- this choice.
[...]
[C]
> - 105: "worse semantics"?  I would not refer to the global store
> semantics as "worse".  It is different and more standard.  I'm not
> actually convinced about whether it is better or worse for equational
> reasoning (there is lot of work now on effective equational reasoning
> in the presence of higher-order state).  You don't demonstrate here
> convincingly that your higher-level semantics is better, just
> different (which is fine).
> 
> Moreover, I got the distinct impression (also at other points in the
> paper) that you have had reviewers complaining to you before that you
> didn't include the global store semantics and you felt obliged to
> include it but didn't want to.  I would recommend toning down the
> editorial remarks about the global store semantics, and just present
> it objectively as an alternative semantics that provides additional
> intuition about what is going on.

These points are well taken, and we will revise accordingly, toning
down as we outlined in our response.

Similarly, we are happy to adjust the structure of the paper following
Reviewer B's suggestion to make the discussion about semantics less of
a detour and more an explicit contribution.

### 1.3 Technical relationship to related work

[B]
> Questions for the response period
> ---------------------------------
> Can the proposed mode system be elaborated to the system in Dreyer
> [2004] or Dreyer (ICFP 2007)? If so, what is the main idea behind the
> translation? If not, why?

We will add (a condensed version of) the explanation from our
response, from the section beginning as follows:

[R]
> A direct comparison between the two formal systems is delicate because
> our language is (moded but) untyped, while Dreyer's system is tightly
> integrated into a type system. However, we believe that any well-typed
> program that is accepted by our mode system could be encoded in
> Dreyer's system (and in practice our check runs after OCaml's
> type-checker, so for our use-case does work with well-typed programs –
> in a more powerful type system than Dreyer's).

[B]
> Are forcing contexts related to the "needed computations" of Chang and
> Felleisen (ESOP 2012)?

We will add (a condensed version of) the explanation from our response,
from the section beginning as follows:

[R]
> Letrec and laziness have commonalities and similar concepts show up
> (including the benefits of using explicit-substitution over
> a global store). In practice the two definitions are similar in
> spirit, but different.

[B]
> Furthermore, regarding the F# approach (2) previously adopted by the
> OCaml compiler, the paper says "we believe that this check as
> originally defined was correct" (Line 76-77). If this approach were
> properly implemented within the OCaml compiler (taking into account
> the typed surface representation and the extensions considered in
> Section 7), what would be the expressiveness gap between it and the
> new approach?

We'll try to more clearly distinguish the various checks (F#'s
syntactic check, OCaml's old syntactic check, our new check) in this
section, and comment explicitly on their relative expressiveness as
outlined in our response

### 1.4 More on Related work

[A]
>   - Line 770 Remark 3.  The idea of using variables as heap locations
>     goes back a long way, at least to Launchbury "A natural semantics
>     for lazy evaluation" (POPL'93), maybe further.
[...]
[A]
>   - As you say, backwards analysis is used for analysis in lazy
>     languages; but "Modular cardinality analysis in theory and
>     practice" (Sergey et al, JFP 2016) would be a more recent
>     citation.  In particular, it uses syntactic typing rules, just
>     as you do. (Cf your remarks line 520.)
> 
> * Syme's paper "Initialising mutually referential abstract objects"
>   talks of the "awkward squad": value-carrying exceptions, concurrency,
>   and continuation.  Are these issues for you?  You should say so.

We plan to address these points in our discussion of related work.

### 1.5 Examples

[X]
> The author response was helpful: all reviewers agree that the
> examples mentioned in the response should help to clarify how the
> type system works, and we expect to see these examples in the final
> version of the paper.

We'll include the examples from the response in the final version.

### 1.6 Empirical evidence

[X]
> For the final version of the paper, I would ask that you give some
> clearer justification for why your type system is sufficiently
> flexible to account for common programming patterns.

We'll incorporate the details from the response about the empirical
evidence that our system is sufficiently flexible in practice:

[R]
> In practice, our system is sufficient to cover the existing use cases
> in OCaml -- it was designed specifically for this.
> 
> To check this we ran it over OPAM, which includes a substantial
> proportion of existing OCaml code; we subsequently released it in
> OCaml 4.06. Six major OCaml releases later there's been only one
> report of a program which was accepted by the old check but not the
> new.
> 
> (The report involved OCaml's labeled-argument functions, which
>  sometimes involve implicit eta expansion; fixing the bug involved
>  changing a Dereference context to a Delay context to take that into
>  account.)
> 
> Note that there is a measurement bias here, in that OCaml packages
> only contain definitions that were accepted by the previous,
> relatively restricted grammatical check. Some extra composability
> falls out of our type-system-like design, as the final example in
> Section 2.3 shows.  However, our goal was not to greatly increase the
> expressivity of OCaml's value definitions, but to get a static check
> that was easier to reason about and evolve in tandem with the
> language.

### 1.7 History

[C]
> - When was the proposed mode system incorporated into OCaml?  Which
> release?

We'll add details about this from the response to the reviews:

[R]
> The initial version of our new system was originally released in OCaml
> 4.06.0 (3 Nov 2017).
> 
> We formalised the new system after its release, and reworked the
> implementation to better match the formalisation.  The updated
> implementation was released in OCaml 4.08.0 (13 June 2019).

### 1.8 The size discipline

[C]
> - §7.1: This section makes it sound as though OCaml may randomly
> reject certain recursive definitions for reasons (having to do with
> the size discipline) not formalized in this paper.  How does this
> relate to the mode system in the paper?

We'll incorporate a version of the explanation from our response:

[R]
> The mode system and the size disciplines are independent. The mode
> system corresponds to a correctness criteria on the operational
> semantics of programs (it will not try to inspect a
> simultaneously-being-defined value), while the size discipline
> corresponds to a restrictive compilation strategy for value recursion
> that leads to rejecting certain definitions. We could formalize the
> size discipline as well (as is done in some works of Hirschowitz et
> al.), but it would be modelled as a separate judgment to be checked
> for well-moded definitions, not as an enrichment of the mode
> judgment. It would also be less portable to other programming
> languages, as their value-representation choices may not require a
> size discipline, or demand a different discipline.

### 1.9 Reduction at a distance

[A]
> * Section 5.1 I wasn't convinced by this whole reduction at a distance
>   business.  It seems much simpler to me to have floating rules
>       (let B in e1) e1  --->  let B in (e1 e1)
>   and similarly for match.  Why is that so bad?  Then you could get
>   rid of all this L stuff.   Make your case!

[C]
> - 602: You mention "reduction at a distance", but I'm still not sure
> exactly what you are referring to.

We'll make our case using the example from the response, from the
section beginning:

[R]
> Extrusion rules are less precise: reduction-at-a-distance is
> admissible in presence of extrusion rules, but some extrusion steps
> are "useless" for the semantics of the term (they do not
> "block" redexes)

### 1.10 Mode composition

[A]
> * Section 4.1 mode composition.  You say (line 356) "If an expression
>   context E[box] uses its hole at mode m...".  But is that truly
>   meaningful?  Isn't a context a mode *transformer*?  That is, if I
>   consume E[e] with mode m, then I consume e with mode m'.  So E
>   transforms m to m'.
> 
>   Indeed this mode composition thing m[m'] seems very
>   mode-transformer-like; but it starts from the suspect idea that it
>   is meaningful to talk of how E[] uses its box without saying how
>   the result is used.

We'll clarify this point in the revision.

### 1.11 Limitations

[A]
> You complain that earlier work is syntactic and ad-hoc. But this is
> pretty syntactic too: it's painful that just a little bit of
> abstractions defeats the system. Surely worth drawing attention to
> this.

[C]
> And even in the response it is still not really clear why these are
> the examples of interest, or why slight tweaks of these examples
> (as suggested in my review, for example -- you didn't discuss the
> example I mentioned in your response) are OK to be rejected.

We'll address this question --- and thus provide more explanation of
the limitations of our system.

Review C provides one specific example; that, or something similar,
will be part of the discussion.

========================================================================

## 2. Comments we plan not to address

There are a small number of comments that we will not be able to
address in the revision, for the reasons given below.

### 2.1 Won't do: formally model advanced OCaml features 

[C]
> Weaknesses
> ----------
> - Paper is motivated by past bad interactions between recursive
> definitions and advanced features of OCaml, but the latter are not
> formally modeled

[C]
> 1. Practicality: The integration into OCaml provides some clear
> evidence for the claim of practical implementability.  There is no
> formal account, however, of the advanced features of OCaml that caused
> soundness problems for the previous syntactic criterion (which is also
> not described in any detail).  There is only Section 7, which is fine
> as far as it goes, but does not really give me high confidence that
> the proposed mode system is sound for full OCaml.

We agree that it would be useful to formally model the advanced
features of OCaml but, unfortunately, it won't be possible to do so in
the revision.

### 2.2 Won't do: give more details about soundness proof

[C]
> Weaknesses
> ----------
> - Main body of paper (ignoring appendix) goes into essentially no
> detail about soundness proof (whose structure is not entirely
> standard), which would help give insight into why the mode system is
> sound

We're following reviewer C's advice here:

[C]
> If you have to prioritize adding details of soundness vs. adding
> concrete examples, add the examples.

### 2.3 Won't do: give details about error messages

[A]
> * Error messages.  What happens if the program is rejected?  You have
>   to cough up a comprehensible error message.  How easy/hard was that?
>   The more expressive the system, the harder it is to explain what's
>   wrong.

Our new check gives the same error messages as OCaml's previous check:
that is, it identifies the location and category of the problem
(let-rec well-formedness), but does not give any further detail.

We believe that it would be posisble to do better, but we have not yet
done any work in this direction, so we don't currently have anything
substantive to say about it.
