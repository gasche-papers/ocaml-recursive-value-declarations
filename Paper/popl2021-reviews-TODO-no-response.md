This file contains comments from the POPL reviews which we do not need
to address in the response (but might in a revision).

[Review A]:
> * Section 4.1 mode composition.  You say (line 356) "If an expression
>   context E[box] uses its hole at mode m...".  But is that truly
>   meaningful?  Isn't a context a mode *transformer*?  That is, if I
>   consume E[e] with mode m, then I consume e with mode m'.  So E
>   transforms m to m'.

[Review A]:
>   Indeed this mode composition thing m[m'] seems very
>   mode-transformer-like; but it starts from the suspect idea that it
>   is meaningful to talk of how E[] uses its box without saying how
>   the result is used.

[Review A]:
> * Syme's paper "Initialising mutually referential abstract objects"
>   talks of the "awkward squad": value-carrying exceptions, concurrency,
>   and continuation.  Are these issues for you?  You should say so.

[Review A]:
> * Error messages.  What happens if the program is rejected?  You have
>   to cough up a comprehensible error message.  How easy/hard was that?
>   The more expressive the system, the harder it is to explain what's
>   wrong.

[Review A]:
> * Motivation.  Section 1.2 suggests that generalised recursion is
>   widely used; but you don't give any gut feel about *why*.  In
>   contrast, Syme's paper "Initialising mutually referential abstract
>   objects" says "One of the main contributions of this paper is to use
>   initialization graphs to present a series of compelling examples of
>   value recursion".  I have not read it in detail, but surely worth
>   citing.  I'm sure there are a limited number of "design patterns"
>   that drive the need for value recursion.
> 
>   Relatedly, line 287 Remark 1 is hard to understand.  Maybe it belongs
>   in a subsection describing the "design patterns" that drive the
>   motivation for generalised recursion?  Or something.

[Review A]:
> * Line 74 "vicious recursive definitions".  You use the term "vicious"
>   frequently (eg line 267), and you even use it as a set in line 696,
>   and use "go vicious" in a theorem line 699, but you never define it.
>   I think you could do better.

[Review A]:
> * Line 167. Your explanation is usually good, but I got lots in this
>   para about contexts and sub-derivations.

[Review A]:
> * Line 178 I wrote down "what is m[Delay]".  Anwered later but I
>   stumbled here.

[Review A]:
> * Line 493ff. You discuss declarative vs algorithmic rules; you say
>   that you use a more algorithmic rule for letrec; and you present a
>   more declarative one.  But you don't say *why* you prefer the more
>   algorithmic one.  It seems to run counter to the tenor of much of
>   your paper.

[Review A]:
> * Section 5.1 and Fig 4.  I think you could explain "weak values"
>   better.  It took me some while to realise that the key "entry point"
>   is that a *value* (K ws) has *weak values* as its arguments.  The
>   intuition is that we can allocate that constructor without its
>   arguments needing to be full values; a variable is fine.  Say more.

[Review A]:
> * Line 613ff. You point to a tiresome problem (infinite unfolding) but
>   you don't say how it is solved.  I don't know what "confluent modulo
>   unfolding.." is, or how it would help.

[Review A]:
> * 5.2 opening sentence -- good, very important! What *exactly* do we
>   mean by "go wrong"?  Cf. my comment on line 64, below.

[Review A]:
> * Line 661 I didn't understand this para about "..can be decomposed
>   into E[x] to isolate...".  What?

[Review A]:
> * Line 687ff.  Example sorely needed.

[Review A]:
> * Lines 17-40.  I was left with a "so-what?" feeling at line 40. Why
>   are you telling me this.  I have to turn the page before I discover.
>   I'd try to get to the payoff sooner in the introduction.

[Review A]:
> * Line 64. "Only ML embodies Milner's dictum".  This is too strong.
>   It all depends what you mean by "go wrong. Lots of well-typed ML
>   programs give the wrong answer or fail to terminate! You are just
>   implicitly growing the definition of "wrong".

[Review A]:
> * Line 79. Segmentation faults.  Give us a clue about how seg-fault
>   can happen.  Basically it's because an object in a recursive group is
>   used before it is fully initialised.

[Review A]:
> * Line 92ff.  I think it's a bit misleading to suggest that 1613/2819
>   packages depend on generalised recursion.  The figure of 74/2819
>   would be more truthful: it is those 74 that would need rewriting
>   somehow if generalised recursion was not available.

[Review A]:
> * Fig 4.  Please use lines or boxes to make it clearer that Fig 4
>   contains four different judgements.  People often put the judgement
>   form in a little box, like a type signature.  It took me a while to
>   decode.  Too much white-space-saving!

[Review A]:
> * Line 602: point specifically to where "binding contexts L are
>   allowed to be presented in the middle of a redex...". It's in
>   Fig 4, rule... ah you don't give names to your rules...what
>   a pity.  The rules for ->hd anyway.

[Review A]:
> * Line 659. Something is syntactically wrong with E[letrec x=t, E'u].

[Review A]:
> * Line 770 Remark 3.  Point to the specific rule(s) where you use
>   variables as heap locations, else this remark is hard to "ground".

[Review B]:
> Terminology: I wonder whether it would be more appropriate to replace
> the the term "vicious," used throughout the paper, with "well-founded
> recursion."

[Review B]:
> Title: Although accurate, the title does not distinguish the work
> from, say, Dreyer 2004 ("A Type System for Well-founded Recursion").
> Perhaps something like "A Simple Syntactic Checker for Well-Founded
> Recursion" would be appropriate?

[Review B]:
> Line 146: I wonder if "Reference" would be preferable to "Guard". The
> former more closely matches the idea and some of the terminology used
> later, and "Delay" can reasonably be described as "guarding" the use
> of a variable with a lambda.

[Review B]:
> Algorithmic typing is discussed in several places: early in Section
> 4.2, Lines 472-483, and Lines 485-506. It may be better to form a
> single "Algorithmic Typing" subsection (not under "Discussion").

[Review B]:
> Conclusion: I'm not sure what "hope" the last sentence is trying to
> convey. For syntax-directed functions to more often be presented as
> inference rules rather than code or other notation?

[Review B]:
> Fig. 1: I realize there would be no production rule for variables, but
> perhaps include only x in the first Term production and add a line for
> "Variables \ni x,y,z" on the right.

[Review B]:
> Line 264, 275: Sometimes "let rec" is displayed bold here but "letrec"
> is not bold in various other places. Sometimes "let" is in italics.

[Review B]:
> Lines 273-278: These explanations seem unnecessary.

[Review B]:
> Line 274: I'm impressed that OCaml list notation "[1;n]" is embraced
> even in this mathematical range notation. :-)

[Review B]:
> Line 282: This parenthetical sentence is a bit too casual. Instead,
> perhaps cite the work in that untyped setting.

[Review B]:
> Remark 1: This felt out of place at this point in the paper. If
> generalized recursion needs to be motivated, then the Introduction
> might be a better place. If not, then perhaps the Discussion.

[Review B]:
> Lines 123-126: The two sentences of the fourth bullet point may be
> combined.

[Review B]:
> Line 156-157: Name the judgment. I recommend "mode checking."

[Review B]:
> Fig. 2: It would be preferable to pick one direction for the mode
> order operator and stick with it. Consider indicating that the second
> and third sections are equivalent views of the same definition. It
> would be nice for the third section to more clearly label the rows m'
> and columns m (perhaps with a diagonally-split cell) in the top-left
> cell. I'd also recommend transposing the table to more easily
> correspond to the m[m'] operation.

[Review B]:
> Sec. 4: Perhaps rename to "A Mode System for ..."

[Review B]:
> Line 326: Here, and throughout the paper, slashes are used to
> juxtapose synonyms. It would be preferable to rewrite these to use
> consistent terminology, using parentheticals or other devices to
> acknowledge alternate terminology.

[Review B]:
> Sec. 4.1: I was surprised not to see the syntax, and operational
> semantics, discussed here before the mode system. (I realized later
> that the semantics is delayed in large part because the presentation
> takes requires an outsize discussion.)

[Review B]:
> Sec. 4.2: Adding rule names and referring to them would help.

[Review B]:
> Line 446: "The let rec b in u ... judgment" ==> "The [so-and-so] rule"

[Review B]:
> Sec. 4.3: Why are the discussions of "Backward type systems" and
> "Modes as modalities" here rather than, say, the Discussion?

[Review B]:
> Double-blind review: Maybe exclude names in Footnote 4 and Line 1003.

[Review B]:
> Fig. 4: The pipe syntax to separate handlers wasn't defined (I think
> it was first used on Line 278).

[Review B]:
> Sec. 5: I think this entire section, rather than just Sec 5.1, should
> be renamed "Operational Semantics."

[Review B]:
> Lines 591, 597: Cite the "most operational semantics" being referred
> to, as well as explicit substitutions.

[Review B]:
> p.13: This is where I really started wondering why the case for a new
> semantics wasn't emphasized until now. I also expected to see a
> discussion to Ariola and Felleisen [1997] (discussed later).

[Review B]:
> Line 659: Is "E'u" either "E'[u]" or "E'[x]"?

[Review B]:
> Line 1100: "Reminding us" as in "resembles" or "informed" the current
> proposal?

[Review B]:
> Strictness analysis: This discussion would benefit from citations.

[Review B]:
> Sec 1.2: I appreciated the empirical evidence for the frequency of
> generalized recursion in practice. These instances could form a basis
> for systematically comparing the expressiveness of the related
> approaches.

[Review B]:
> Sec 7.1: I wonder if a more detailed presentation about these
> practical concerns would be a better use of space than arguing for the
> need of a new source level semantics (which could make for a nice
> separate paper, as suggested above).

[Review B]:
> Sec 7.2: This feature and concern seems less fundamental than that of
> the previous subsection, so I think the current length and level of
> detail is appropriate.


[Review C]:
> - line 44: You start talking about recursion being "safe". I would
> clarify (at least informally) up front what you mean by "safe".

[Review C]:
> - 171-173: At this point at least, mode composition is not well
> motivated.  I had to keep reading to eventually figure it out.

[Review C]:
> - 528: I didn't get much out of this "modes as modalities" section.
> I'm not sure what point you were trying to make.

[Review C]:
> - 659: I can't parse the expression at the beginning of this line, I
> wonder if it is a typo.
