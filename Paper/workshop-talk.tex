\input{\jobname.cfg}
\newcommand{\acmart}{\False}

\documentclass[usenames,dvipsnames,table]{beamer}
\usepackage[utf8]{inputenc}

\usepackage{reversion}
\usepackage{mylistings}
\usepackage{mycomments}
\usepackage{mymath}
\usepackage{mybiblio}
\usepackage{myhyperref}
\usepackage{notations}
\usepackage{myrefs}
\usepackage{mybeamer}

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{\hfill\insertframenumber\hfill\vspace{3mm}}

\title{A right-to-left type system\\ for mutually-recursive value definitions}
\date{\today}
\author{Alban Reynaud, \textbf{Gabriel Scherer}, Jeremy Yallop}
\institute{Parsifal, Inria Saclay, France}

\begin{document}

\begin{frame}
  \titlepage

\begin{center}
  \includegraphics[height=3cm]{pictures/Alban_Reynaud.jpg}
  \hfill
  \includegraphics[height=3cm]{pictures/Gabriel_Scherer.jpg}
  \hfill
  \includegraphics[height=3cm]{pictures/Jeremy_Yallop.jpg}
\end{center}
\end{frame}

\begin{frame}[fragile]
\begin{lstlisting}
let rec /*fac*/ = function
| 0 -> 1
| n -> n * /*fac*/ (n - 1);;
(* val $\color{blue}{\text{fac}}$ : int -> int = <fun> *)
fac 8;;
(* - : int = $\color{blue}{\text{40320}}$ *)

let rec /*ones*/ = 1 :: /*ones*/;;
(* val $\color{blue}{\text{ones}}$ : int list = [1; <cycle>] *)
List.nth ones 10_000;;
(* - : int = $\color{blue}{\text{1}}$ *)

let rec /*alot*/ = 1 + /*alot*/;;
(* $\color{red}{\text{Error}}$: This kind of expression is not allowed
   as right-hand side of `let rec' *)
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Almost-killer app: toy interpreter}

\visible<5->{
  \lstinline{Factorial := FunRec(f,n): if n=0 then 1 else n*f(n-1)}
}

|Adder := Fun(x): Fun(y): x+y|\pause

\[ \mathtt{Adder(1)}
  \to^\star
  \mathsf{closure}([x \mapsto 1],\;y \mapsto x+y) \]\pause


\vfill

\begin{lstlisting}
type ast $~$ = Var of var | ... | Fun of var * expr
type value = ... | Closure of env * var * expr
and env $~~$ = (var * value) list$\pause$

let rec eval env = function
| Var x -> List.assoc x env
| ...
| Fun (x, t) -> Closure(env, x, t)$\pause$
| FunRec (f, x, t) -> $\pause$
  (* Closure((f, ?) :: env, x, t) *) $\pause$
  let rec clo = Closure((f,clo) :: env, x, t) in clo
\end{lstlisting}
\end{frame}

\begin{frame}{State of the OCaml art}

\href{file:///home/gasche/Prog/ocaml/github-trunk/manual/manual/htmlman/manual023.html}%
  {OCaml manual $\to$ Language Extensions $\to$ Recursive definitions of values}

\pause

\vfill

Complex syntactic description.

Not composable.

Hard to trust.

Did not age very well with new language features.

\vfill\pause

\href{https://caml.inria.fr/mantis/view.php?id=7231}{PR\#7231}:
check too permissive with nested recursive bindings

\href{https://caml.inria.fr/mantis/view.php?id=7215}{PR\#7215}:
Unsoundness with GADTs and let rec

\href{https://caml.inria.fr/mantis/view.php?id=4989}{PR\#4989}:
Compiler rejects recursive definitions of values

\href{https://caml.inria.fr/mantis/view.php?id=6939}{PR\#6939}:
Segfault with improper use of let-rec and float arrays
\end{frame}

\begin{frame}[fragile]{State of the OCaml art}

\href{https://caml.inria.fr/mantis/view.php?id=7231}{PR\#7231}:
check too permissive with nested recursive bindings

\begin{lstlisting}
   let rec r = let rec x () = r
                   and y () = x ()
                in y ()
          in r "oops"
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{State of the OCaml art}

\href{https://caml.inria.fr/mantis/view.php?id=7215}{PR\#7215}:
Unsoundness with GADTs and let rec

\begin{lstlisting}
   let is_int (type a) : (int, a) eq =
     let rec (p : (int, a) eq) =
       match p with Refl -> Refl
     in p
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{State of the OCaml art}
\href{https://caml.inria.fr/mantis/view.php?id=4989}{PR\#4989}:
Compiler rejects recursive definitions of values

\begin{lstlisting}
   let rec f = let g = fun x -> f x in g
$~$
$~$
$~$
\end{lstlisting}

%  [Notable: this was a feature wish from long before the new check
%   was implemented, and was difficult to implement with the old
%   approach (marked as "won't fix any time soon" by Xavier), but
%   falls out naturally from the new check.]
\end{frame}


\begin{frame}[fragile]{State of the OCaml art}
\href{https://caml.inria.fr/mantis/view.php?id=6939}{PR\#6939}:
Segfault with improper use of let-rec and float arrays

\begin{lstlisting}
  let rec x = [| x |]; 1. in ()
$~$
$~$
$~$
\end{lstlisting}
\end{frame}

% \section{Solution}
% \frame{\sectionpage}

\begin{frame}{The typical approach}
  We propose a \emph{type system} to check recursive value definitions.

  \vfill

  Our types are one of five \emph{access modes} $m$, with a typing
  judgment $\der \Gamma t m$. A recursive declaration is safe if
  the mode of the recursive variables is gentle enough.

  The typing rules are formulated so that an algorithm can easily be
  extracted.

  \vfill

  We wrote the corresponding code; it landed in the OCaml compiler
  (\href{https://github.com/ocaml/ocaml/pull/556}{\#556}, April 2016;
  \href{https://github.com/ocaml/ocaml/pull/1942}{\#1942}, July 2018),
  fixing more bugs than we introduced.

  \vfill

  \href{https://github.com/ocaml/ocaml/blob/4.08/typing/rec_check.ml\#L543-L555}{Implementation}
\end{frame}

\newcommand{\modedescr}[1]{
\begin{description}
\item[$\Ignore$]: $1$
\item[$\Delay$]: $\lam y #1$, $\app {\mathsf{lazy}} #1$.
\item[$\Guard$]: $\constr K {(#1)}$
\item[$\Return$]: $#1$, $\letin y e #1$
\item[$\Dereference$]: $1 + #1$, $\app #1 y$, $\app f #1$.
\end{description}
}

\begin{frame}[fragile]{Access modes}
  The mode of $x$ in $t$ is: \modedescr{x}

\pause\vfill

Total order: $\Ignore \prec \Delay \prec \Guard \prec \Return \prec \Dereference$.

  \vfill\pause

  \begin{mathpar}
    \begin{array}{l@{\quad}r@{\;\vdash\;}l}
      \letrec {f = \lam n {n * \app f {(n - 1)}}}
      & \of f \Delay & \lam n {n * \app f {(n - 1)}} : \Return
      \\
      \letrec {o = \kwd{Cons}(1, o)}
      & \of o \Guard & \constr {Cons} {(1, o)} : \Return
      \\
      \letrec {x = 1 + x}
      & \of x \Dereference & 1 + x : \Return
      \\
      \letrec {x = \letin y x y}
      & \of x \Return & \letin y x y : \Return
    \end{array}
  \end{mathpar}

  \vfill\pause
  
  Safety criterion: recursive variables must have mode $\Guard$ or less.
\end{frame}

\begin{frame}{Mode typing judgment $\der \Gamma t m$}

Using $t$ at mode $\Guard$: $K(t)$.

\vfill

Two readings of the judgment $\der {\of x {m_x}} t m$:
\begin{description}
\item[left-to-right]: If $x$ is safe at mode $m_x$, then $t$ can be used at $m$.
\item[right-to-left]: Using $t$ at $m$ requires using $x$ at $m_x$.
\end{description}

\vfill

Right-to-left / backward reading: $t$, $m$ inputs, $\Gamma$ output

\begin{mathpar}
  \infer*
  {\Alt<-1>{}{\infer*{ }{\der {\emptyset} 1 \Guard}}
   \\
   \Alt<-1>{}{\infer*
   {\Alt<-2>{}{\infer*{ }{\der {\of x {\Alt<-3>{?} \Dereference}} x \Dereference}}}
   {\der {\of x {\Alt<-4>{?} \Dereference}} {\app{\kwd{fst}} x} \Guard}}}
  {\der {\of x {\Alt<-5>{?} \Dereference}} {\constr {\kwd{Pair}} {(1, \app{\kwd{fst}} x)}} \Return}
\end{mathpar}
%1
\pause%2
\pause%3
\pause%4
\pause%5
\pause%6
\end{frame}

\begin{frame}{Access modes algebra}
  The mode of $x$ in $C[x]$: the mode action of the context $C[\hole]$.

  \modedescr{\hole}

\pause\vfill

Mode composition: $C[C'[\hole]]$ has mode action $\mcomp m {m'}$.\pause
\begin{mathpar}
  \begin{array}{lll}
    \mcomp \Ignore m & = & \Ignore \quad = \quad \mcomp m \Ignore
    \\
    \mcomp \Delay {m > \Ignore} & = & \Delay
    \\
    \mcomp \Guard \Return & = & \Guard
    \\
    \mcomp \Guard {m \neq \Return} & = & m
    \\
    \mcomp \Return m & = & m
    \\
    \mcomp \Dereference {m > \Ignore} & = & \Dereference
    \\
\end{array}
\end{mathpar}
\deemph{
$\mcomp \Dereference \Delay \neq \mcomp \Delay \Dereference$
\hfill
$\app f (\lam x \hole), \lam x {(\app f \hole)}$
}
\end{frame}

\begin{frame}{Access mode typing rules}
  \begin{small}
  \begin{mathpar}
      \infer{ }
      {\der {\Gamma, \of x m} x m}

      \infer
      {\der \Gamma t m\\
        m \succ m'}
      {\der \Gamma t {m'}}
      \\
      \infer
      {\der {\Gamma, \of x {m_x}} t {\mcomp m \Delay}}
      {\der \Gamma {\lam x t} m}

      \infer
      {\der {\Gamma_t} t {\mcomp m \Dereference} \\
        \der {\Gamma_u} u {\mcomp m \Dereference}}
      {\der {\envsum {\Gamma_t} {\Gamma_u}} {\app t u} m}
      \\
      \infer
      {\fam i {\der {\Gamma_i} {t_i} {\mcomp m \Guard}}}
      {\der {\envbigsum {\fam i {\Gamma_i}}} {\constr K {\fam i {t_i}}} m}
      
      \text{(pattern matching rules...)}
      \\
      \infer
      {{\begin{array}{c}
       \Alt<-2>{}{m_{x \in t} \leq \Guard}
       \\
       \Alt<-1>{}{\der {\Gamma_t, x:m_{x \in t}} t \Return}
       \end{array}}
       \\
       {\begin{array}{c}
       \Alt<-5>{}{m'_{x \in u} \defeq \max(m_{x \in u}, \Guard)}
       \\
       \der {\Gamma_u, x:m_{x \in u}} u m
       \end{array}}}
      {\der {\Alt<-3>{\qquad?}{\mcomp {\Alt<-5>{\bgalert<5>{m_{x \in u}}}{m'_{x \in u}}} {\Gamma_t} + \Gamma_u}} {\letrecin {x = t} u} m}
  \end{mathpar}
  \end{small}
%1
\pause%2
\pause%3
\pause%4
\pause%5
\pause%6
\end{frame}

\begin{frame}{Soundness theorem}
\begin{center}
  If $\der \emptyset t \Return$

  and $t \to^\star {t'}$

  then $t'$ is not going horribly wrong.

\vfill\pause

What's a good operational semantics for $\kwd{let rec}$?
\end{center}

\vfill\pause

A source-level approach to $\kwd{let rec}$: explicit substitutions.
~\\
\citet*{hirschowitz-compilation-2003,hirschowitz-compilation-2009}
~\\
\citet*{dynamic-semantics-2008}
\end{frame}

\begin{frame}
\begin{mathpar}
  \set{Vicious} \defeq \{ \plug {\forcing E} x
                  \mid \nexists v, \inctx x v {\forcing E} \}
\end{mathpar}

\begin{theorem}
  If
  \[ \der \emptyset t \Return \]
  and
  \[ t \to^\star {t'} \]
  then
  \[ t' \notin \set{Vicious} \]
\end{theorem}

\begin{proof}
  Subject Reduction.
\end{proof}
\end{frame}

\begin{frame}{Related Work}
  \begin{description}
  \item[Backward analyses] We describe them as type systems. Syntax!
  \item[Modal type theories] This is an instance of one -- uni-typed.
  \item[Modal type theories for (co)recursion] We have a nice inference algorithm.
  \item[Degrees] Elaborate systems for objects and ML functors,
    need to accept more programs. Not uni-typed.
  \item[Graphs as types] We don't.
  \item[Operational semantics] Best order vs. worst order.
  \end{description}

  \vfill

  For more details, see our full paper:

  \begin{center}
    \url{https://arxiv.org/abs/1811.08134}
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \huge{End.}
  \end{center}

  \bibliography{letrec}
\end{frame}

\begin{frame}{Bonus slide: reduction example}
  \Alt<3>{(xs = \constr {\kwd{Cons}} {(x, xs)}) \in E[\hole]
    \qquad \text{(would work even if $\letrec$ at toplevel)}}
  {\Alt<5-6>{(\letrecin {\fam i {x_i = v_i}} \dots)}{}}
  \vfill

  \begin{small}
    \begin{mathpar}
      \begin{array}{rl}
        &
          \match
          {\left(
          \begin{array}{l}
            \letrecin {xs = \constr {\kwd{Cons}} {(1, xs)}} {}
            \\\qquad
            \bgalert<2-3>{xs}
          \end{array}\right)}
          {\left[
          \begin{array}{l@{\to}l}
          \constr {\kwd{Nil}} {} & \constr {\kwd{None}} {}
          \\
          \constr {\kwd{Cons}} {(y, ys)} & \constr {\kwd{Some}} {(ys)}
          \end{array}\right.}
        \\[1.5em]
        \rew{}{}
        &
        \pause%2
        \pause%3
          \match
          {\left(
          \begin{array}{l}
            \bgalertcyan<5-6>{\letrecin {xs = \constr {\kwd{Cons}} {(1, xs)}} {}}
            \\\qquad
            \bgalert<3,5-7>{\constr {\kwd{Cons}} {(1, xs)}}
          \end{array}\right)}
          {\left[
          \begin{array}{l@{\to}l}
          \constr {\kwd{Nil}} {} & \constr {\kwd{None}} {}
          \\
          \bgalert<5-7>{\constr {\kwd{Cons}} {(y, ys)}}
          & \constr {\kwd{Some}} {(ys)}
          \end{array}\right.}
        \\[1.5em]
        \pause%4
        \rew{}{}
        &
        \pause%5
        \pause%6
          \bgalertcyan<6>{\letrecin {xs = \constr {\kwd{Cons}} {(1, xs)}} {}}
          ~
          {\pause%7
           \pause%8
           \constr {\kwd{Some}} {(xs)}}
      \end{array}
    \end{mathpar}
  \end{small}
\end{frame}

\begin{frame}{Bonus slide: Source term syntax}
\begin{mathpar}
  \begin{array}{l@{~}r@{~}l}
    \set{Terms} \ni t, u & \bnfeq & x, y, z \\
    & \bnfor & \letrecin b u \\
    & \bnfor & \lam x t %  \\
    % &
      \bnfor % &
               \app t u \\
    & \bnfor & \constr K {\fam i {t_i}} % \\
    % &
      \bnfor % &
               \match t h \\ \\
  \end{array}

  \begin{array}{l@{~}r@{~}l}
    \set{Bindings} \ni b & \bnfeq & {\fam i {x_i = t_i}} \\
    % \\
    \set{Handlers} \ni h & \bnfeq & {\fam i {\clause {p_i} {t_i}}} \\
    % \\
    \set{Patterns} \ni p, q & \bnfeq & \constr K {\fam i {x_i}} \\
  \end{array}
\end{mathpar}
\end{frame}

\begin{frame}%{Source operational semantics}
\begin{small}
\begin{mathpar}
  \begin{array}{r@{~}r@{~}l}
    \set{Values} \ni v & \bnfeq
    & \lam x t
      \bnfor \constr K {\fam i {w_i}} 
      \bnfor \plug L v \\
    \set{WeakValues} \ni w & \bnfeq
    & x \bnfor v \bnfor \plug L w \\
    \set{ValueBindings} \ni B & \bnfeq
    & \fam i {x_i = v_i} \\
    \set{BindingCtx} \ni L
    & \bnfeq
    & \square \bnfor \letrecin B L \\
    \\\pause
    \set{EvalCtx} \ni E
    & \bnfeq
    & \square \bnfor \plug E F
    \\
    \set{EvalFrame} \ni F & & \\
  \end{array}
  \begin{array}{r@{~}r@{~}l}
    F & \bnfeq & \app \hole t
    \;\bnfor\; \app t \hole \\
    & \bnfor & \constr K {(\fam i {t_i}, \hole, \fam j {t_j})} \\
    & \bnfor & \match \hole h \\
    & \bnfor & \letrecin {b, x = \hole, b'} u \\
    & \bnfor & \letrecin B \hole \\
  \end{array}\pause

  \infer
  {\inctx x v E}
  {\rew {\plug E x} {\plug E v}}

  \infer
  {\rewhead t {t'}}
  {\rew {\plug E t} {\plug E {t'}}}\pause

  \infer
  {\inframe x v F \quad\vee\quad \inctx x v E}
  {\inctx x v {\plug E F}}

  \infer{\inbinding x v B}
  {\inframe x v {\letrecin B \hole}}

  \infer{\inbinding x v (b \cup b')}
  {\inframe x v {\letrecin {b, y = \hole, b'} u}}\pause

  \infer{ }
  {\rewhead {\app {\plug L {\lam x t}} v} {\plug L {\subst t {\by v x}}}}

  \infer
  { }
  {\rewhead
    {\match {\plug L {\constr K {\fam i {w_i}}}}
      {(\ldots \mid \clause {\constr K {\fam i {x_i}}} u \mid \ldots)}}
    {\plug L {\subst u {\fam i {\by {w_i} {x_i}}}}}}
\end{mathpar}
\end{small}
\end{frame}

\begin{frame}
\begin{mathpar}
  \begin{array}{r@{~}r@{~}l}
    \set{ForcingFrame} \ni {\forcing F}
    & \bnfeq & \app \hole v
    \;\bnfor\; \app v \hole
    \\
    & \bnfor & \match \hole h
    \\
    & \bnfor & \letrecin {b, x = \hole, b'} t
    \\
    \set{ForcingCtx} \ni {\forcing E}
    & \bnfeq & \forcing F
    \;\bnfor\; \plug E {\forcing E}
    \;\bnfor\; \plug {\forcing E} {L} \\
  \end{array}

  \set{Vicious} \defeq \{ \plug {\forcing E} x
                  \mid \nexists v, \inctx x v {\forcing E} \}
\end{mathpar}


\end{frame}

\begin{frame}{Bonus slide: mutual recursion}
\begin{small}
\begin{mathpar}
    \infer
    {\derbinding {\fam i {\of {x_i} {\Gamma_i}}} b \\
      \fam i {m'_i} \defeq \fam i {\max(m_i, \Guard)} \\
      \der {\Gamma_u, \fam i {\of {x_i} {m_i}}} u m}
    {\der
      {\envsum
        {\envbigsum {\fam i {\mcomp {m'_i} {\Gamma_i}}}}
        {\Gamma_u}}
      {\letrecin b u}
      m}

    \infer
    {\fam {i \in I}
      {\der
        {\Gamma_i, \fam {j \in I} {\of {x_j} {m_{i,j}}}}
        {t_i}
        \Return}
     \\
     \fam {i,j} {m_{i,j} \preceq \Guard}
     \\\\
     \fam i {\Gamma'_i =
       \envsum {\Gamma_i} {\envbigsum {\fam j {\mcomp {m_{i,j}} {\Gamma'_j}}}}}
    }
    {\derbinding
      {\fam {i \in I} {\of {x_i} {\Gamma'_i}}}
      {\fam {i \in I} {x_i = t_i}}}
\end{mathpar}
\end{small}
\end{frame}

\end{document}
