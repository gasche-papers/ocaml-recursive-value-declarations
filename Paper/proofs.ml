type mode =
| Ignore
| Delay
| Guard
| Return
| Dereference

let rank = function
  | Ignore -> 0
  | Delay -> 1
  | Guard -> 2
  | Return -> 3
  | Dereference -> 4

let prec m1 m2 = rank m1 <= rank m2

let join m1 m2 = if rank m1 >= rank m2 then m1 else m2

let compose m' m = match m', m with
  | Ignore, _ | _, Ignore -> Ignore
  | Dereference, _ -> Dereference
  | Delay, _ -> Delay
  | Guard, Return -> Guard
  | Guard, ((Dereference | Guard | Delay) as m) -> m
  | Return, Return -> Return
  | Return, ((Dereference | Guard | Delay) as m) -> m

let modes = [Ignore; Delay; Guard; Return; Dereference]
let return x = [x]
let bind li f = List.flatten (List.map f li)
let guard b f = if b then f () else []

let all li = List.for_all (fun b -> b) li

let compose_associative =
  assert (all (
    bind modes @@ fun m1 ->
    bind modes @@ fun m2 ->
    bind modes @@ fun m3 ->
    return (compose m1 (compose m2 m3) = compose (compose m1 m2) m3)))

let ignore_absorbing =
  assert (all (
    bind modes @@ fun m ->
    return (compose m Ignore = Ignore && Ignore = compose Ignore m)))

let return_identity =
  assert (all (
    bind modes @@ fun m ->
    return (compose Return m = m && m = compose m Return)))

let compose_idempotent =
  assert (all (
    bind modes @@ fun m ->
    return (compose m m = m)))

let localize_order =
  assert (all (
    bind modes @@ fun m ->
    bind modes @@ fun m'1 ->
    bind modes @@ fun m'2 ->
    guard (prec m'1 m'2) @@ fun () ->
    return (prec (compose m m'1) (compose m m'2))))

let localize_max =
  assert (all (
    bind modes @@ fun m ->
    bind modes @@ fun m'1 ->
    bind modes @@ fun m'2 ->
    return (compose m (join m'1 m'2)
            = join (compose m m'1) (compose m m'2))))
