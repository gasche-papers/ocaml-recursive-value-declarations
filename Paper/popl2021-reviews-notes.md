### In defence of our local-store semantics

[JY: we can concede most of the revewiers' points here without any
 technical changes and without acknowledging anything that would
 damage the chances of acceptance: "The reviewers' point about the
 tone of Section 6 is well-taken, and we will revise accordingly.  As
 Reviewer A says, it's reasonable for readers to be interested in an
 operational semantics that is connected to the actual behaviour of
 programs.  Similarly, we are happy to adjust the structure of the
 paper following Reviewer B's suggestion to make the discussion about
 semantics less of a detour and more an explicit contribution."]

[Review B]:
> Was the less common approach chosen for this
> paper specifically because it helps reason about the soundness of mode
> checking? If so, how?

> p.647: "We found it suprisingly subtle." How does this factor
> contribute to the overall tradeoff about whether or not to use an
> existing semantics to establish soundness for the mode checker?

[Review C]:
> - 105: "worse semantics"?  I would not refer to the global store
> semantics as "worse".  It is different and more standard.  I'm not
> actually convinced about whether it is better or worse for equational
> reasoning (there is lot of work now on effective equational reasoning
> in the presence of higher-order state).  You don't demonstrate here
> convincingly that your higher-level semantics is better, just
> different (which is fine).

This argument is bogus:

[JY: I don't think the reviewers are especially arguing in favour of
 the global store semantics, except for Reviewer A's point that it's
 close to the machine.  And nobody's opposed to the local-store
 semantics.  But the reviewers do ask for specific technical reasons
 about the superiority of local-store semantics *for this
 system/proof* --- in fact, this is a main concern for Reviewer B ---
 so we should focus on that.]

- It has been demonstrated in the 90s that local-store semantics are
  simpler, nicer, easier to reason about than global-store semantics
  for call-by-need, and that recursion can be handled similarly. We
  don't use technical tools because they are "familiar", we use the
  best tools for the job. The change-avert attitude in those reviews
  is part of what kept the inferior global-store semantics "standard"
  between the 90s and today, and it holds our field back. People keep
  publishing paper using outdated tools, their work would be strictly
  improved by using local-store semantics.

- It is easier to do Kripke-indexed logical relation proofs than ever
  before, but the best you can do if you start from our stateful
  semantics is to use those fine tools to prove that you get the same
  reasoning power than with the local-state semantics. (The latter
  does not use *mutable* state, so any extra nice properties you could
  prove in today's rich logics could be proved there just as well.)

Maybe we should add even more arguments in favor of the local-store
semantics, in particular point out that it makes the soundness proof
possible, while the global-store semantics would make the proof much
harder.

> Moreover, I got the distinct impression (also at other points in the
> paper) that you have had reviewers complaining to you before that you
> didn't include the global store semantics and you felt obliged to
> include it but didn't want to.  I would recommend toning down the
> editorial remarks about the global store semantics, and just present
> it objectively as an alternative semantics that provides additional
> intuition about what is going on.

[Review C]:
> - Secondary contribution: The paper also makes the secondary
> contribution of presenting two semantics for recursive definitions and
> connecting them.  I liked this.  However, it seems (at least based on
> how the authors describe it) that this is mostly derivative of prior
> work by Nordlander et al. and Hirschowitz et al., so I hesitate to put
> too much weight on this aspect of the paper.

[Gabriel] we can of course tone the paper down to hide any
frustration. (I'm not sure we need to say this explicitly.) There will
no shoving the clean semantics into a separate paper. I think that the
discussion is somewhat misguided:

- The argument that the global-store semantics is closer to the
  implementation is not necessarily a good one. Otherwise we would be
  writing all our papers in assembly language! It is closer to the
  machine, but also messier and harder to reason about; having clean
  semantics that are detached from machine details is a key value of
  our community, and it feels strange to have to argue for it.

- The argument that the global-store semantics should be used because
  it is more well-known leaves me speechless. It was demonstrated in
  the 90s that explicit-substitution semantics were better for
  laziness. At the time, people kept using global-store semantics
  because they knew them better, and here we are in 2020 having to
  argue that this old idea is still better than the status quo.

I don't know what I [Gabriel] can write about this in a persuasive
way, in addition to the discussion already in the paper.

I think that a good approach would be to say that the clean semantics
is a much better vehicle to do the soundness proof. In fact, I am not
even sure that we can do the semantics on the lower-level system,
without adding much of the complication present in the simulation
relation.

In terms of how "derivative" this work is: we had to redo the proofs
from scratch, differently from the proof of Hirschowitz, so they are
"new" (and they took a serious effort to get right); but they are not
*novel* in the sense that it was already previously known that
local-store semantics are correct wrt. global-store semantics, and our
paper does not add much new here. (So why do reviewers keep asking for
global-store semantics? Sigh.)

### Shouldn't we support abstraction?

[Review A]:
> One huge advantage of a type system is that it supports abstraction
> [...] it's painful that just a little bit of abstractions defeats
> the system

[Review C]:
> For example, unless I'm confused, it seems that the following
> recursive definition would be rejected by the proposed mode system
> (and at least OCaml 4.10.0 rejects it):
> 
> ```
> let rec f =
>   let h g x = g x in
>   let f' x = f x in
>     h f'
> in ...
> ```
> 
> I could be wrong, but I believe this example would be accepted by
> nearly any prior type system for recursive definitions that I can
> think of.

[TODO]

### Practicality, and soundness for full OCaml

[Review C]:
> - Paper is motivated by past bad interactions between recursive
> definitions and advanced features of OCaml, but the latter are not
> formally modeled
[...]
> 1. Practicality: The integration into OCaml provides some clear
> evidence for the claim of practical implementability.  There is no
> formal account, however, of the advanced features of OCaml that caused
> soundness problems for the previous syntactic criterion (which is also
> not described in any detail).  There is only Section 7, which is fine
> as far as it goes, but does not really give me high confidence that
> the proposed mode system is sound for full OCaml.

[Gabriel] When people write paper on the C++ memory model, or a new
feature in Scala, they don't start from a full formal semantics of C++
or Scala (to my knowledge none exists). In absence of a complete
formalization of these aspects of OCaml, we don't have a choice but to
work on a core language that exhibits the salient points of the
system, and then discussing extensions informally.
