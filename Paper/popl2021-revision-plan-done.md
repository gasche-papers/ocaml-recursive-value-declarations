# Revisions: submission #349

This document describes the changes that we've made following the
revision plan that we sent to our shepherd.

### 1.1 Title

We've changed "type system" to "mode system" and added "practical".

### 1.2 Backpatching

We've toned down our editorial remarks: for example, we no longer
describe the global store semantics as "worse", and we've rephrased
the comments originally in lines 706ff, and 748 so that they're no
longer dismissive.  The exposition now presents the global store
semantics more objectively "as an alternative semantics that provides
additional intuition about what is going on", as reviewer C asked.

We also looked at adjusting the structure of the paper following
Reviewer B's suggestion to make the discussion about semantics less of
a detour and more an explicit contribution, but we weren't sure how to
do so without significantly overhauling things.  We hope that the
description of the advantages of local-store semantics will serve as a
useful contribution, now that we have removed the rather charged
language that the reviewers objected to.

### 1.3 Technical relationship to related work

We've outlined in the related work section how to elaborate our
system into Dreyer's.

We've explained (again in the related work section) how our forcing
contexts are related to Chang and Felleisen (ESOP 2012).

We've updated the text in the introduction to more clearly distinguish
the various syntactic checks (F#'s syntactic check, OCaml's old
syntactic check, our new check).

### 1.4 More on Related work

We've added references to Launchbury's POPL'93 paper and (in the
remarks on backwards analysis and laziness) to Sergey et al's JFP 2017
paper.

We've noted (in the related work section) the issues that F# has with
the awkward squad (particularly concurrency, which turns out to be the
only squad member addressed in detail in the published description),
and explained that they arise from the implicit laziness in his
scheme.

### 1.5 Examples

We've included the examples from the response.

### 1.6 Empirical evidence

We've incorporate the details from our response about the empirical
evidence that our system is sufficiently flexible in practice.

### 1.7 History

We've added (to the introduction) the details about the release
history from our author response.

### 1.8 The size discipline

We've incorporated a version of the explanation from our response.

### 1.9 Reduction at a distance

We've updated the section on "Head reduction" to incorporate the the
example from our author response.

### 1.10 Mode composition

We've tweaked the wording slightly, but on closer examination we found
that the point was already addressed in the introduction ("This
judgment uses access modes to classify not just variables, but also
the constraints imposed on a subterm by its surrounding context."),
and made precise in the technical description of mode composition.

### 1.11 Limitations

We've (condensed and) included in the section on modes as modalities
some of the discussion about abstraction that we posted on HotCRP.
