# Revision plan: submission #349

### 1.3 Technical relationship to related work

B> 3. the "degrees" of Boudol [2001], Bardou [2005], and Hirschowitz and
B> Lenglet [2005]; and

B> What is the gap in expressiveness of these different approaches,
B> for example, in terms of the benchmark programs those examined in
B> the OCaml libraries and other programs that are beyond reach of the
B> proposed system?

========================================================================

### Introduction issues

A> * Motivation.  Section 1.2 suggests that generalised recursion is
A>   widely used; but you don't give any gut feel about *why*.  In
A>   contrast, Syme's paper "Initialising mutually referential abstract
A>   objects" says "One of the main contributions of this paper is to use
A>   initialization graphs to present a series of compelling examples of
A>   value recursion".  I have not read it in detail, but surely worth
A>   citing.  I'm sure there are a limited number of "design patterns"
A>   that drive the need for value recursion.

A> * Line 74 "vicious recursive definitions".  You use the term "vicious"
A>   frequently (eg line 267), and you even use it as a set in line 696,
A>   and use "go vicious" in a theorem line 699, but you never define it.
A>   I think you could do better.

A> * Lines 17-40.  I was left with a "so-what?" feeling at line 40. Why
A>   are you telling me this.  I have to turn the page before I discover.
A>   I'd try to get to the payoff sooner in the introduction.

C> - line 44: You start talking about recursion being "safe". I would
C> clarify (at least informally) up front what you mean by "safe".

### Overview (Section 2) issues

A> * Line 178 I wrote down "what is m[Delay]".  Anwered later but I
A>   stumbled here.

B> Section 2.3: The first and fourth examples seem reasonable, even if
B> synthetic. But the second and third sound like implementation issues.
B> For the second example, the syntactic check would have failed for the
B> original term in the surface syntax, right? For the third example,
B> could the syntactic check be extended in a simple way to account for
B> unboxed float arrays as a special case?

B> Line 156-157: Name the judgment. I recommend "mode checking."

C> - 171-173: At this point at least, mode composition is not well
C> motivated.  I had to keep reading to eventually figure it out.

### Give names to judgements and rules (and lay out figures better)

B> Sec. 4.2: Adding rule names and referring to them would help.
B> 
B> Line 446: "The let rec b in u ... judgment" ==> "The [so-and-so] rule"

A> * Line 602: point specifically to where "binding contexts L are
A>   allowed to be presented in the middle of a redex...". It's in
A>   Fig 4, rule... ah you don't give names to your rules...what
A>   a pity.  The rules for ->hd anyway.

A> * Fig 4.  Please use lines or boxes to make it clearer that Fig 4
A>   contains four different judgements.  People often put the judgement
A>   form in a little box, like a type signature.  It took me a while to
A>   decode.  Too much white-space-saving!

### Remark 1

A> Relatedly, line 287 Remark 1 is hard to understand.  Maybe it belongs
A> in a subsection describing the "design patterns" that drive the
A> motivation for generalised recursion?  Or something.

B> Remark 1: This felt out of place at this point in the paper. If
B> generalized recursion needs to be motivated, then the Introduction
B> might be a better place. If not, then perhaps the Discussion.

### Other

A> * Line 493ff. You discuss declarative vs algorithmic rules; you say
A>   that you use a more algorithmic rule for letrec; and you present a
A>   more declarative one.  But you don't say *why* you prefer the more
A>   algorithmic one.  It seems to run counter to the tenor of much of
A>   your paper.
A> 
A> * Section 5.1 and Fig 4.  I think you could explain "weak values"
A>   better.  It took me some while to realise that the key "entry point"
A>   is that a *value* (K ws) has *weak values* as its arguments.  The
A>   intuition is that we can allocate that constructor without its
A>   arguments needing to be full values; a variable is fine.  Say more.
A> 
A> * Line 613ff. You point to a tiresome problem (infinite unfolding) but
A>   you don't say how it is solved.  I don't know what "confluent modulo
A>   unfolding.." is, or how it would help.
A> 
A> * 5.2 opening sentence -- good, very important! What *exactly* do we
A>   mean by "go wrong"?  Cf. my comment on line 64, below.
A> 
A> * Line 661 I didn't understand this para about "..can be decomposed
A>   into E[x] to isolate...".  What?
A> 
A> * Line 687ff.  Example sorely needed.

A> * Line 770 Remark 3.  Point to the specific rule(s) where you use
A>   variables as heap locations, else this remark is hard to "ground".

B> Terminology: I wonder whether it would be more appropriate to replace
B> the the term "vicious," used throughout the paper, with "well-founded
B> recursion."

B> Algorithmic typing is discussed in several places: early in Section
B> 4.2, Lines 472-483, and Lines 485-506. It may be better to form a
B> single "Algorithmic Typing" subsection (not under "Discussion").
B> 
B> p.647: "We found it suprisingly subtle." How does this factor
B> contribute to the overall tradeoff about whether or not to use an
B> existing semantics to establish soundness for the mode checker?
B> 
B> Line 1092: "Sounds like" sounds like too informal a comparison,
B> especially for something "similar in spirit." Are there examples
B> that demonstrate differences in expressiveness?
B> 
B> Lines 1105-1110: If the usage-polymorphic type doesn't restrict
B> callers, what does the user need "to think about"? Why not always
B> choose usage-polymorphism where possible?
B> 
B> Conclusion: I'm not sure what "hope" the last sentence is trying to
B> convey. For syntax-directed functions to more often be presented as
B> inference rules rather than code or other notation?

B> Fig. 1: I realize there would be no production rule for variables, but
B> perhaps include only x in the first Term production and add a line for
B> "Variables \ni x,y,z" on the right.
B> 
B> Line 264, 275: Sometimes "let rec" is displayed bold here but "letrec"
B> is not bold in various other places. Sometimes "let" is in italics.
B> 
B> Line 282: This parenthetical sentence is a bit too casual. Instead,
B> perhaps cite the work in that untyped setting.
B> 
B> Fig. 2: It would be preferable to pick one direction for the mode
B> order operator and stick with it. Consider indicating that the second
B> and third sections are equivalent views of the same definition. It
B> would be nice for the third section to more clearly label the rows m'
B> and columns m (perhaps with a diagonally-split cell) in the top-left
B> cell. I'd also recommend transposing the table to more easily
B> correspond to the m[m'] operation.

B> Sec. 4.1: I was surprised not to see the syntax, and operational
B> semantics, discussed here before the mode system. (I realized later
B> that the semantics is delayed in large part because the presentation
B> takes requires an outsize discussion.)
B> 
B> Sec. 4.3: Why are the discussions of "Backward type systems" and
B> "Modes as modalities" here rather than, say, the Discussion?
B> 
B> Fig. 4: The pipe syntax to separate handlers wasn't defined (I think
B> it was first used on Line 278).
B> 
B> Sec. 5: I think this entire section, rather than just Sec 5.1, should
B> be renamed "Operational Semantics."
B> 
B> Lines 591, 597: Cite the "most operational semantics" being referred
B> to, as well as explicit substitutions.
B> 
B> p.13: This is where I really started wondering why the case for a new
B> semantics wasn't emphasized until now. I also expected to see a
B> discussion to Ariola and Felleisen [1997] (discussed later).
B> 
B> Line 1100: "Reminding us" as in "resembles" or "informed" the current
B> proposal?
B> 
B> Strictness analysis: This discussion would benefit from citations.
