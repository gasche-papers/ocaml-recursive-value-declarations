#!/bin/bash
MAIN=letrec
BIB=letrec

mkdir arxiv || { echo "error, you need to (rmdir arxiv)"; exit 1; }
rm -f arxiv.zip

cp $MAIN.tex $MAIN.cfg arxiv/

cp $BIB.bib arxiv/ # (unused) source .bib, for reference
stat $BIB.bbl > /dev/null || { echo "you need to run bibtex first"; exit 1; }
cp $BIB.bbl arxiv/

cp *.sty arxiv/

cp acmart.cls arxiv/
cp proofs.ml arxiv/

(
    echo "pdflatex $MAIN.tex"
    echo "pdflatex $MAIN.tex"
    echo "pdflatex $MAIN.tex"
) > arxiv/build.sh

zip -r arxiv arxiv
echo "feel free to test the packed source in arxiv/, archive is arxiv.zip"
