We thank the reviewers for their constructive and detailed feedback.

We would like to first answer our reviewers' explicit questions for
the response period (questions with short answers first), then comment
on some other salient points of their excellent feedback.  (It is not
possible to address every point raised in the reviews here, but we
plan to take careful account of all the feedback when revising the
paper.)

## Explicit questions for the response period

[Review C]:
> - When was the proposed mode system incorporated into OCaml?  Which
> release?

The initial version of our new system was originally released in OCaml
4.06.0 (3 Nov 2017).

We formalised the new system after its release, and reworked the
implementation to better match the formalisation.  The updated
implementation was released in OCaml 4.08.0 (13 June 2019).

[Review C]:
> - Does your mode system actually cover all the use cases that showed up
> in OCaml packages?  Intuitively, why is it sufficiently liberal?

In practice, our system is sufficient to cover the existing use cases
in OCaml -- it was designed specifically for this.

To check this we ran it over OPAM, which includes a substantial
proportion of existing OCaml code; we subsequently released it in
OCaml 4.06. Six major OCaml releases later there's been only one
report of a program which was accepted by the old check but not the
new.

(The report involved OCaml's labeled-argument functions, which
 sometimes involve implicit eta expansion; fixing the bug involved
 changing a Dereference context to a Delay context to take that into
 account.)

Note that there is a measurement bias here, in that OCaml packages
only contain definitions that were accepted by the previous,
relatively restricted grammatical check. Some extra composability
falls out of our type-system-like design, as the final example in
Section 2.3 shows.  However, our goal was not to greatly increase the
expressivity of OCaml's value definitions, but to get a static check
that was easier to reason about and evolve in tandem with the
language.

[Review B]:
> Line 665: Are forcing contexts related to the "needed computations" of
> Chang and Felleisen (ESOP 2012)?

Letrec and laziness have commonalities and similar concepts show up
(including the benefits of using explicit-substitution over
a global store). In practice the two definitions are similar in
spirit, but different. For example in our system

    f (fst □)

is a forcing context, but the hole is not in "needed position" for
call-by-need.

Intuitively, needed contexts correspond to positions where "any choice
of reduction order must force here to make progress", whereas forcing
contexts correspond to positions where "some choices of reduction
order may force here and blowup if the value is initialized".

[Review B]:
> Can the proposed mode system be elaborated to the system in Dreyer
> [2004] or Dreyer (ICFP 2007)? If so, what is the main idea behind the
> translation? If not, why?

A direct comparison between the two formal systems is delicate because
our language is (moded but) untyped, while Dreyer's system is tightly
integrated into a type system. However, we believe that any well-typed
program that is accepted by our mode system could be encoded in
Dreyer's system (and in practice our check runs after OCaml's
type-checker, so for our use-case does work with well-typed programs –
in a more powerful type system than Dreyer's).

The idea would be roughly that

- if in the context Γ of a derivation (Γ ⊢ t : Return) in our system,
  we have (x : Dereference), then in Dreyer's system the corresponding
  effect variable X would need to be an ambient capability
  (Γ ⊢ t : τ [T] with X∈T)

- on the other hand, if we have (x : Return) or a more permissive
  mode, then we would ask for the corresponding term variable in the
  Dreyer-style derivation to have type box{X}(T)

So for example

    x: Dereference |- x + 1 : Return

would lead to

    X, x ⊢ x + 1 : Int [X]

but

    x : Guard |- { t = x } : Return

would lead to

    X, x:box{X}(τ) ⊢ { t : x } : some-record-type [∅]

The whole derivation would ask the right-hand-side of recursive
definition to be at non-boxed types (so (x:Return |- x:Return) does
not give a valid derivation without unboxing), without any of the
effect variables of the recursively-defined variable in the ambient
context.

[Review C]:
> Can you give any examples to clearly demonstrate why this fancy mode
> system is needed, how it works, and why a simpler criterion would not
> suffice for handling OCaml packages?

Given the reviewers' feedback, we will change our presentation choices
to add more examples in the paper. In the meantime, below are examples
demonstrating first the nuances between the various modes, and then
the "let" rule without and with mutual recursion.

Examples on the nuances of `Return` vs. `Guard`, `Guard` vs. `Delay`, `Delay` vs. `Ignore`:

1. 
   ```
   let rec x = x
   ```
   should be rejected.  Corresponding mode judgment:
   ```
       x: Return |- x : Return
   ```
   (`x` is stricter than `Guard` in the context, our check fails)

2. 
   ```
     let rec x = { self = x }
     let rec f = fun x -> f x
   ```
    should be accepted.  Corresponding mode judgments:
   ```
       x : Guard |- { self = x } : Return
       f : Delay |- ... : Return
   ```
       (our check passes)

3. 
   ```
     let rec x = g { self = x }
     let rec f = g (fun x -> f x)
   ```
   should be rejected (g may dereference the self field of its argument, call its argument).  Corresponding mode judgments:
   ```
       g: Dereference, x : Dereference |- g { self = x } : Return
       g: Dereference, f : Dereference |- g (fun x -> f x) : Return
   ```
    thanks to the mode composition rules
   ```
         Dereference[Guard] = Dereference
         Dereference[Delay] = Dereference
   ```
    (our check fails)

4. In contrast to the previous examples:
   ```
    let rec x = { self = g x }
   ```
   gives
    ```
     g: Dereference, x:Dereference |- { self = g x } : Return
    ```
   (check fails)
   from `Guard[Dereference] = Dereference`, but
    ```
     let rec f = fun x -> g (f x)
    ```
   gives
    ```
     g : Delay, f: Delay |- fun x -> g (f x) : return
    ```
     (checks passes)
   from `Delay[Dereference] = Delay`

5. For Ignore: notice that if we have
   ```
    x : Delay |- t : Return
   ```
   then we have
    ```
     x : Dereference, f : Dereference |- f t : Return
    ```
   but if we have
    ```
     x : Ignore |- t : Return
    ```
   then we still have
    ```
     x : Ignore, f : Dereference |- f t : Return
    ```
   So a declaration such as
    ```
     let rec x = f t
    ```
   would be rejected if
  ```
  (x : Delay |- t : Return),
  ```
   but accepted if
  ```
  (x : Ignore |- t : Return).
  ```

6. Examples for the "let" rule.  
   Example A:
  ```
    x: Delay |- let t = fun y -> (x, y) in { self = t } : Return
  ```
   we have
  ```
      x : Delay |- fun y -> (x, y)
  ```
    in the definition of t and
  ```
     t : Guard |- { self = t }
  ```
    so the final context is
  ```
      Guard[x : Delay] = x : Delay
  ```
   Example B:
  ```
    g: Dereference, x: Dereference |- let t = fun y -> (x, y) in g t : Return
  ```
    we have
  ```
      x : Delay |- fun y -> (x, y) : Return
  ```
    in the definition of t and
  ```
     t : Derefrence, g: Dereference |- g t : Return
  ```
    so the final context is
  ```
      Dereference[x : Delay], g:Dereference = x :Dereference, g:Dereference
  ```

7. Example with mutual recursion:
  ```
      let rec t = x
      and u = { self = t }
      and v = fun () -> (u, y)
  ```
   We have:
   ```
       x:Return |- x : Return                           (for t = ...)
       t:Guard |- { self = t } : Return                 (for u = ...)
       u:Delay, y:Delay |- fun () -> (u, y) : Return    (for v = ...)
   ```
   So we have to find Γ'₁, Γ'₂, Γ'₃ such that
   ```
       Γ'₁ = (x : Return) + ∅  (t does not use any mutually-recursive name)
       Γ'₂ = ∅ + Guard[Γ'₁]    (u uses t in Guard mode)
       Γ'₃ = (y : Delay) + Delay[Γ'₂]
   ```
     the fixpoint solution is
   ```
       Γ'₁ = x:Return
       Γ'₂ = x:Guard
       Γ'₃ = x:Delay, y:Delay
   ```
   For example we would have
   ```
       x:Return, y:Delay |-
         let rec t = x
         and u = { self = t }
         and v = fun () -> (u, y)
         in t
   ```
     rejected, but
   ```
       x:Guard, y:Delay |-
         let rec t = x
         and u = { self = t }
         and v = fun () -> (u, y)
         in u
      accepted.
   ```

Regarding whether a simpler criterion would suffice, OCaml programmers
do in practice make use of the expressiveness offered by the existing
check, as we note in Section 1.2:

> We found 309 distinct examples of such definitions in around 74
> packages. The definitions variously made use of local bindings and
> other local constructs (such as local exception definitions),
> evaluation of sub-terms, record and variant construction and laziness,
> in both singly- and mutually-recursive bindings.

As we go on to say, our new system also supports all these instances
of generalized recursive definitions.

## Additional questions

[Review B]:
> Furthermore, regarding the F# approach (2) previously adopted by the
> OCaml compiler, the paper says "we believe that this check as
> originally defined was correct" (Line 76-77). 
> If this approach were properly implemented within the OCaml compiler
> (taking into account the typed surface representation and the
> extensions considered in Section 7), what would be the
> expressiveness gap between it and the new approach?

There's a small misunderstanding here: the F# approach was not adopted
in OCaml.  The core of F#'s approach is to treat recursive definitions
as lazy, which can lead to run time failures (if the value of a
recursive definition is demanded during while the definition is
currently being forced).  To mitigate this problem of run time
failures, F# subsequently introduced an additional syntactic check,
which rejects some (but not all) vicious definitions statically.
OCaml also previously had a syntactic check for vicious definitions,
but it is not based on lazy evaluation --- it is instead based on
OCaml's eager semantics.

There is an expressiveness gap between F#'s check and our new check:
F#'s check accepts `mfib` in our introduction, but rejects `mfib'`,
both of which are accepted by our check; on the other hand it accepts
some definitions which fail at run time, which our check does not.

There is also a small expressiveness gap between our check and the
original OCaml check, as the examples in Section 2.3 show.  Some of
these differences (particularly PR#7215) could indeed be eliminated if
the original check were implemented on the typed syntax rather than
the untyped intermediate representation, but it would be difficult to
eliminate others (particularly PR#4989) with the original syntactic
approach.

In summary: our aim was to replicate the expressiveness of the
original syntactic check as far as possible while eliminating known
problems.  The empirical evidence suggests that we have not lost any
useful expressiveness, and the soundness proof shows that we have
plugged some holes.  Additionally, we have gained some
compositionality (as shown, e.g. by PR#4989) and a principled
framework for extending to new language features and informally
reasoning about those parts of the language that are not formally
modelled.

[Review C]:
> - §7.1: This section makes it sound as though OCaml may randomly
> reject certain recursive definitions for reasons (having to do with
> the size discipline) not formalized in this paper.  How does this
> relate to the mode system in the paper?

The mode system and the size disciplines are independent. The mode
system corresponds to a correctness criteria on the operational
semantics of programs (it will not try to inspect a
simultaneously-being-defined value), while the size discipline
corresponds to a restrictive compilation strategy for value recursion
that leads to rejecting certain definitions. We could formalize the
size discipline as well (as is done in some works of Hirschowitz et
al.), but it would be modelled as a separate judgment to be checked
for well-moded definitions, not as an enrichment of the mode
judgment. It would also be less portable to other programming
languages, as their value-representation choices may not require a
size discipline, or demand a different discipline.

[Review A]:
> * Section 5.1 I wasn't convinced by this whole reduction at a distance
>   business.  It seems much simpler to me to have floating rules
>       (let B in e1) e1  --->  let B in (e1 e1)
>   and similarly for match.  Why is that so bad?  Then you could get
>   rid of all this L stuff.   Make your case!

[Review C]:
> - 602: You mention "reduction at a distance", but I'm still not sure
> exactly what you are referring to.

Extrusion rules are less precise: reduction-at-a-distance is
admissible in presence of extrusion rules, but some extrusion steps
are "useless" for the semantics of the term (they do not
"block" redexes). For example the reduction

    (let B in λx.t) u ~>* let B in t[u/x]

is admissible in both systems, but the "useless" reduction

    (let B in x) u ~>* let B in (x u)

is not present in our system. Reduction-at-a-distance is
a somewhat-recent innovation of the lambda-calculus-and-rewriting
community, and it tends to make definitions crisper *and* to simplify
proofs. (For an example of beneficial use of reduction-at-a-distance
in previous work from the rewriting community, see the at-a-distance
presentation of the pi-calculus in "Evaluating functions as
processes", Beniamino Accatoli, 2013.)

[Review A]:
> Lines 706ff, and 748.  You are very dismissive of these readers,
> presenting Section 6 as a concession to the feeble-minded. But
> there's a Very Good Reason that your readers want section 6: the
> operational semantics of Fig 4 *is not what is executed*.

[Review C]:
> - 105: "worse semantics"?  I would not refer to the global store
> semantics as "worse".  It is different and more standard.  I'm not
> actually convinced about whether it is better or worse for equational
> reasoning (there is lot of work now on effective equational reasoning
> in the presence of higher-order state).  You don't demonstrate here
> convincingly that your higher-level semantics is better, just
> different (which is fine).

These points are well taken, and we will revise accordingly.
Similarly, we are happy to adjust the structure of the paper following
Reviewer B's suggestion to make the discussion about semantics less of
a detour and more an explicit contribution.
 
Finally, on the choice of operational semantics. Overall our reviewers
seem to agree that our choice of a source-level semantics is sensible,
especially given the validation coming from the
compilation-correctness result with respect to a mutable-store
semantics. We tried to summarize our overall position as follows:

- The calculi for call-by-need of 1995 and 1996 (Ariola, Felleisen,
  Maraist, Odersky, Wadler) demonstrate that the explicit-substitution
  approach gives semantics that are easier to reason about than the
  previous store-passing, lower-level semantics. (This was already
  claimed in those papers.)

- Given a choice to make, we prefer to use the technically-superior
  approach to the better-known approach.

- Explicit substitions are not fringe either. They are used in several
  previous work on letrec, including the works of Hirschowitz and
  coauthors. They are well-known in other research communities; in
  computational logic, see for example "Classical by-need" by Pédrot
  and Saurin, 2016, which also uses reduction-at-a-distance.

- We shall tone down the discussion of the two approaches in the
  paper, following the reviewer recommendations.

- We believe that the type-preservation proof would be harder to make
  with the global-store semantics. With a source-level calculus, the
  reduction of a subterm is purely local, as is its typing derivation,
  we do not need to consider the context (we only reason globally in
  the error case). In a lower-level calculus you need to reason about
  the fragment of the store mentioned in the subterm you are looking
  at, which can be non-trivial (especially as there is no unique
  ownership here).
